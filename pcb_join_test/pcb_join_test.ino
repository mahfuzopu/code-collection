//#include <SoftwareSerial.h>
//SoftwareSerial mySerial(11, 12); // RX, TX
#include "WiFly.h"
#include <EEPROM.h>
#include "var.h"                  /* All variables are declared here */
//////////////////////////////////////////////////////////////////////////////////////////
String getID()
{
  String default_id = String("0000");
  int is_id = EEPROM.read(10) ; 
  if(is_id == 1)
  {
    setID();
  }
  else 
  {
    eeprom_id = default_id;
  }
  return eeprom_id ;
}
void setID()
{
  int add = 12 ;
  char start = (char)EEPROM.read(11);
  if(start=='#')
  {
    char c = (char)EEPROM.read(add);
    eeprom_id = String("");
    while(c!='#' &&add<20)
    {
      c = (char)EEPROM.read(add);
      if(c=='#')break;
      eeprom_id+= c;
      add++;
    }
  }
  Serial.print("new id assigned=");
  Serial.println(eeprom_id);
  
  ROOM_ID[0] = String(eeprom_id);
}

/*************************************************************************************/

#include "devicemanager.h"        /* set device properties etc */
#include "communication.h"        /* communication with serial ports*/
#include "switch.h"               /* pcb switch connection checking etc */
#include "connection.h"           /* connection with outside world  */
/************************************************************************************
 *
 **  parse the server ip and port and return ok or nok 
 *
 ************************************************************************************/
boolean init_ip_port(byte *buf){
  int pos;
  boolean flag = true ;
  s_ip    = String("");
  s_port  = String("");

  for(pos = 1 ; buf[pos]!='#' ; pos++){
    if(buf[pos]==':'){ flag=false; continue ; }
    (flag)?s_ip+=(char)buf[pos]:s_port+=(char)buf[pos];
  }
  /* OK check */
  if(s_ip.length() <= 15 && s_ip.length() >= 11 && s_port.length()>=1 && s_port.length()<=5)return true ;
  else return false ;
}
/*******************************************************************************************
*
* *  Show the software serial output
*
********************************************************************************************/
void inline SHOW_(String s)
{
    Serial.println(s);
}
void inline d_ms(int t)
{
  delay(t);
}
/////////////////////////////////////////////////
//                   SETUP
////////////////////////////////////////////////
void setup()
{
  
  FOR(N)pinMode(relay[i],OUTPUT);
  
  SpiSerial.begin(9600);
  Serial.begin(9600);
  
  SHOW_("Hello my World!");
  
  init_sw();                         /* initialize the pcb switches  defined in swith.h*/
  
  configure_wifly();                 /* initialize the wifly connection defined in communication.h */
 
  SHOW_("After Wifly Configure");
  
  rs();
  
  d_ms(5000);
  
  rs();
  isLearningMood  = true ;
  isJoiningMood   = !check_join();    /* check join in comunicaiton.h  */
  master_timeout = millis();
  //time = millis();
  
}
/////////////////////////////////////////////////////////
//        packet action
//////////////////////////////////////////////////////////
void send_update_to_server(String pac){
  String rid = pac.substring(1,5);
    if(rid==ROOM_ID[0])
    {
      int did = pac.charAt(8) - '0';
      int c1 = pac.charAt(9)-'0';
      boolean c2 = getDeviceOnOff(did);
      boolean c3 = (c1==1)?true:false;
      if(c3^c2)
      {
        //update server
        setDeviceON(did,c3);
        send_device_info_to_server(pac);
        
      }
    }
}
void do_what_is_told_to_do_with_packet(String pac){
 // A 0001 0 1 100 00
  String rid = pac.substring(1,5);
  
  //mySerial.println("Room id: " + rid);
  
  if(rid==ROOM_ID[0]){
    int did = pac.charAt(8) - '0';
    int c = pac.charAt(9)-'0';
    /////////////////////////////////////////////////
    if(did==0){
      if(c==0)digitalWrite(relay[0],LOW);
      else if(c==1)digitalWrite(relay[0],HIGH);
    }
    else if(did==1){
      if(c==0)digitalWrite(relay[1],LOW);
      else if(c==1)digitalWrite(relay[1],HIGH);
    }
    else if(did==2){
       if(c==0)digitalWrite(relay[2],LOW);
      else if(c==1)digitalWrite(relay[2],HIGH);
    }
    else if(did==3){
       if(c==0)digitalWrite(relay[3],LOW);
       else if(c==1)digitalWrite(relay[3],HIGH);
    }
  }
}

String create_packet(String room_id,int device_id,boolean on_off,int othr){
  //String d_id = String(device_id);
  String r_id = getID();
  String on   = (on_off)?"1":"0";
  //String other= String(othr);
  String d_id = "0000000" + String(device_id);
  int len = d_id.length();
  d_id = d_id.substring(len-4,len);
  
  String other = "0000000" + String(othr);
  int len2 = other.length();
  other = other.substring(len2-3,len2);
  
  String pac = String("R");
  pac +=  r_id + "" + d_id + "" + on + "" + other + ":";
  
  //mySerial.println("Constructed packet::========================");
  SHOW_(pac);
  //mySerial.println("Constructed packet::========================");
  
  return pac ;
}
//////////////////////////////////////////////////////
//        Switch checking in main loop
//////////////////////////////////////////////////////
void check_join_status()
{
   if((millis()-time)> (AP_CHECK_TIME)){
    time = millis();
    rs();
    isJoiningMood = !check_join();
    
    if(isJoiningMood)
    {
      isLearningMood = true ;
      SHOW_("Not Associated");
      rs();
      rs();
      cmd(); 
      rs();
      rs();
      com("join AndroidAP"); ex(); d_ms(100);
      rs();
      rs();
      com("exit"); ex(); d_ms(100);
      rs();  
    }
    else
    {
      SHOW_("Associated");
      isJoiningMood = false ;
    }
  }
}

//////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
//                    LOOP
////////////////////////////////////////////////////////////////////////////////////////////
void loop(){
  
  boolean isChange[N];
  FOR(N)isChange[i] = false;
  byte _buffer[100];
  
  check_join_status();
  
  /************ this is for debug purpose___ will be comment out before final *********************/
 // if( isJoiningMood &&  isLearningMood)   {  SHOW_("in learning & joining mood");                }
 // if( isJoiningMood && !isLearningMood)   {  SHOW_("in Joining Mood but not in learning mood");  }
 // if(!isJoiningMood &&  isLearningMood)   {  SHOW_("in learning mood.But not in Joining Mood");  }
 // if(!isJoiningMood && !isLearningMood)   {  SHOW_("System Running Cool!");                      }
  /***************************************************************************************************/
  
  /*
  if(!isJoiningMood && !isLearningMood && (millis()-master_timeout)>(MASTER_TIME_OUT)){  /*   CHECK IF THE MASTER IS ACTIVE AND SENDING THE UDP RESPONSE */
 //   SHOW_("MASTER TIMEOUIT");
 //    isLearningMood = true ;
 // }
     
  if(!isJoiningMood)
  {
    while(SpiSerial.available())
    {
      char b = (char)SpiSerial.read();
      
      Serial.print("<");
      Serial.print(b);
      Serial.print(">");
            /*
      Skip the starting and ending  */
      SKIPING_STARTING_CHAR(b)        /*  see #define skip *OPEN* and *CLOS* 
      
      */
      if(isLearningMood)
      {
        if(b=='1')
        {
            SHOW_("if b==1 ");
            _buffer[1] = b ; int pos = 2 ;
            while(SpiSerial.available())
            {
              char k = (char)SpiSerial.read();
              delay(2);
              SKIPING_STARTING_CHAR(k)
              if(k=='.' || k==':' || ( k>='0' && k<='9'))_buffer[pos++] = k ;
            }
            _buffer[0] = _buffer[pos] = '#';
            for(int JJ = 1 ; _buffer[JJ]!='#';JJ++)Serial.println(_buffer[JJ]);
            
            if(init_ip_port(_buffer))              /*   CHECK IF IP AND PORT SEND BY MASTER IS OK OR NOT   */
            {
              String str = getDeviceString(ROOM_ID[0],ROOM_NAME[0]);
              SHOW_("Device String : " + str);
              server_int(s_ip,s_port,str);

           }
        }
      }
      else 
      {  // is not in learning mood
        if(b=='A')
        {
            SHOW_("if b==A");
            String pac_from_server = String("A");
            while(SpiSerial.available())
            {
                char c = (char)SpiSerial.read();
                delay(2);
                Serial.print(c);
                SKIPING_STARTING_CHAR(c)
                pac_from_server+= c;
            }
            Serial.println("SERVER PACKET:");
            Serial.println(pac_from_server);
             //mySerial.println("Bdoing kach kach to pac!");
            
            
            String ack_pac = String("R");
            ack_pac+= pac_from_server.substring(1);
            do_what_is_told_to_do_with_packet(pac_from_server);
            send_update_to_server(ack_pac);
            //Serial.println(ack_pac);
            Serial.println("DONE kach kach to pac!");
            //Serial.flush();
                //defiene in fucntions.h
            
          }
          if(b=='K')
          {
           // master_timeout = millis();
             isLearningMood = true ;
          }
          if(b=='X')
          {
            // id asssignmend
            Serial.print("Here in b==X");
            EEPROM.write(11,'#');
            EEPROM.write(10,1);
            int add = 12;   
            while(SpiSerial.available())
            {
              char u = (char)SpiSerial.read();
              SKIPING_STARTING_CHAR(u)
              EEPROM.write(add,u);
              delay(2);
              add++;
              Serial.println(u);   
            }
            EEPROM.write(add,'#');
            setID();
          }
       }
    }
  }
  else
  {
    rs();
  }
 
  /// check the switch status and on/off accordingly
  FOR(N)
  {
    if(cur_sw[i]^prev_sw[i])
    {
      cur_sw[i] = prev_sw[i];
      if(cur_sw[i])digitalWrite(relay[i],HIGH);
      else digitalWrite(relay[i],LOW);
      setDeviceON(i,(cur_sw[i]?(true):(false)));
      isChange[i] = true;
    }
  }
  if(!isJoiningMood && !isLearningMood)
  {
    FOR(N)
    {
        if(isChange[i])
        {
          String pac = create_packet(ROOM_ID[0],i,cur_sw[i],0);
          send_device_info_to_server(pac);
        }
        isChange[i] = false;
    }
  }
  ////////////////////////////upto change ////////////////////////////
  check_sw();  /* check for switch change  */
  
}

