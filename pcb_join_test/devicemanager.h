#include "var.h"
void add_device(struct device div){
  
  device_list[device_counter].name   = div.name;
  device_list[device_counter].id     = device_counter ; 
  device_list[device_counter].on     = div.on ;
  device_list[device_counter].other  = div.other;
 
  device_counter++ ;
}
struct device newDevice(String name, int id,boolean on , int other){
  struct device temp ; 
  temp.name = String(name);
  temp.id = id ; 
  temp.on = on ;
  temp.other = other;
  
  return temp;
}
void setDeviceON(int id,boolean b){
  if(id<device_counter)device_list[id].on = b ;
}
boolean getDeviceOnOff(int id){
  for(int i = 0 ; i < device_counter ; i++){
    if(device_list[i].id == id){
       return device_list[i].on;
    }
  }
  return false ;
}
void show_device_status(){
  for(int i = 0 ; i < device_counter ;i++){
    Serial.print(device_list[i].id);
    Serial.print(":");Serial.print(device_list[i].name);
    if(device_list[i].on==false)Serial.println(":OFF");
    else Serial.println(":ON");
  }
}
String getDeviceString(String room_id,String room_name){
  String s = String("");
  s = s + getID() + "," + room_name ;
  
  //Serial.print("Decice counter==");
  //Serial.println(device_counter);
  
  for(int i = 0 ; i < device_counter ; i++){
    String name = device_list[i].name ;
    String id   = String(device_list[i].id) ;
    String on   = (device_list[i].on)?"1":"0" ;
    String other= String(device_list[i].other) ;
    //Serial.print("Counter=");Serial.println(i);
    //Serial.println(s);
    s = s + "," + name + "," + id + "," + on + "," + other + ":" ;
  }
  //Serial.println("in get device detail functions:");
 // Serial.println(s);
  return s ;
}

