#ifndef SWITCH_H
#define SWITCH_H
/////////////////////////////////////////////////
//      Switch Opeations
/////////////////////////////////////////////////

void init_sw(){
  
  FOR(N){ v_sw[i]=0;}
  
  FOR_FOR(10,N)
  {
    v_sw[j] +=analogRead(sw[j]);delay(2);
  }
  FOR(N)
  {
    v_sw[i] = (v_sw[i]*3.3)/10230;
  }
  FOR(N)
  {
    if(v_sw[i]<3.29)
    {
      prev_sw[i] = true ; digitalWrite(relay[i],HIGH);
    }
    else 
    {
      prev_sw[i] = false ; digitalWrite(relay[i],LOW);
    }
    cur_sw[i] = prev_sw[i];
    struct device dev = newDevice(load[i],0, prev_sw[i],0);
    add_device(dev);
  }
    
}
void check_sw(){
  FOR(N)v_sw[i]=0;
  int SR = 20; 
  
  FOR_FOR(SR,N)
  {
    v_sw[j]+= analogRead(sw[j]);
    delay(2);
  }
  FOR(N)
  {
    v_sw[i] = (v_sw[i]*3.3)/(1023*SR);
  }
  FOR(N)
  {
    if(v_sw[i]<3.29)prev_sw[i]=true;
    else prev_sw[i]=false;
  }
}

/////////////////////////////////////////////////////////////////////////
#endif
