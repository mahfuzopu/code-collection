#ifndef VAR_H
#define VAR_H

///////////////////////////////////////////////////
//          DATA SECTION
////////////////////////////////////////////////////

static String  ROOM_ID[]   = { "0002" } ;
static const String  ROOM_NAME[] = { "Room-2" }; 
String s_ip    = String("");              // server ip
String s_port  = String("");              // server port

#define N 4
#define SKIPING_STARTING_CHAR(b)  if(b=='*' || b=='O' || b=='P' || b=='E' || b=='N')continue ; else if(b=='C' || b=='L' || b=='O' || b=='S')continue ;
#define FOR(M) for(int i = 0; i < M ;i++)
#define FOR_FOR(P,Q) for(int i=0;i<P;i++)for(int j=0;j<Q;j++)

const int relay[] = {3,4,5,6};                            // pinout of relay connected in output mode.

volatile boolean isLearningMood = true ;                   /* operating mode 1 */
volatile boolean isJoiningMood  = true ;                   /* operating mode 2 */

const int sw[N] = {A0,A1,A2,A3};                            // analog input of pcb mechanical switch

unsigned long time ;                                        // counter of association time
long master_timeout;                                        // counter of master time out
#define AP_CHECK_TIME  1000*60*3                               // Association time out.check after each 5s if there it is associated or not
#define MASTER_TIME_OUT  1000*60*2                          // one mints

double v_sw[N];                                             // calculate the analog values of each switch switch

boolean prev_sw[N] = { false , false , false ,false} ;      //switch previous state

boolean cur_sw[N] = { false , false , false , false} ;      //switch current state

const String load[N] = {"Load1","Load2","Load3","Load4"};  //name the load initially

#define K 20
struct device{
  String name ;
  int id ; 
  boolean on;
  int other;
};
struct device device_list[K];   // holds the device list
int device_counter = 0 ;        // set the id of devices

const char EC = 0x0d;  //(carriage return for entering commands)

String eeprom_id = String("0000");

#endif
