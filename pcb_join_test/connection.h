#ifndef CONNECTION_H
#define CONNECTION_H
#include "var.h"
boolean wait_for_ack(){
  
  Serial.println("Waiting for ACK");
  
  long ack_timeout = millis();
 // delay(200);
  int b_count = 0 ,count  = 0 ; 
  while((millis()-ack_timeout)<3000){
    if(SpiSerial.available()){
      char b = (char)SpiSerial.read();
      Serial.println(b);
      if(b=='x')count++ ;
      if(count==3){ return true ; }
    }
    b_count++;
  }
  return false;
}
void server_int(String ip,String port,String msg){
    
  rs();
  cmd();
  rs();
  
  //mySerial.println("Before sending TCP");
  String con = String("open");
  con += " "+ ip + " " + port;
  com(con);  
  ex();
  delay(250);
  rs();
  
  Serial.println("Before sending msg");
  SpiSerial.println(msg);
  delay(250);
  Serial.println("after sending msg");
  //Serial.flush();
  rs();
  delay(10);
  
  if(wait_for_ack()){
     isLearningMood = false ;
     master_timeout = millis();
  }
  else
  {
     isLearningMood = true ;
  }
  
  rs();
  com("exit"); ex(); delay(100); rs();
}
void my_connect_to_server(String ip,String port,String msg){
  
  rs();
  cmd();
  rs();
  
  //mySerial.println("Before sending TCP");
  String con = String("open");
  con += " "+ ip + " " + port;
  com(con);  
  ex();
  delay(250);
  //rs();
  
  SpiSerial.println(msg);
  delay(10);
  
  
  //rs();
  com("exit"); 
  ex(); delay(100); 
  rs();
  
  //Serial.flush();
  //rs();
  //delay(50);
  //com("close");
  //ex();
  //Serial.flush();
  //delay(100);
  
  
  //rs();
  
  //mySerial.println(msg);
  //mySerial.println("AFTER TCP");
  //rs();
  //relay_run(3);
  
 // Serial.println("close");
  
  
  
  /*
  delay(500);
  rpSPI();
  
  Serial.print("open "); Serial.print(ip); Serial.print(" "); Serial.print(port);
  Serial.println();
  
  rpSPI();
  
  Serial.println(msg);
  
  rpSPI();
  */
}

void send_device_info_to_server(String packet){ 
  
  //mySerial.println("");
  //mySerial.println("before sending device change ...");
  
 /*********************************************/
  //send_packet_to_master(s_ip,s_port,ROOM_ID,packet);
  String room_iid = getID();
  int s1 = room_iid.toInt();
  int s2 = s_port.toInt();
  int s3 = s1 + s2 ; 
  //String p1 = String(s3);
  String p1 = String("8888"); //new addition
  //mySerial.print("send to port=");
  //mySerial.println(p1);
  
  my_connect_to_server(s_ip,p1,packet);
  
 /*********************************************/
//  Serial.println("Before sending packet");
  //mySerial.println("Packet info:");
  //mySerial.println(packet);
  /*
  if(wait_for_ack())
  {
    mySerial.println("ACK!");
  }
  else 
  {
     mySerial.println("Error found!");
  }
  mySerial.println("");
  */
}

#endif
