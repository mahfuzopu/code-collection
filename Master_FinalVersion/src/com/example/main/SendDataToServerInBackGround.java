package com.example.main;

import com.mahfuz.outdoor.connection.JSONParser;

import android.os.AsyncTask;
import android.util.Log;

public class SendDataToServerInBackGround extends AsyncTask<String, Void , Boolean>{
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}
	@Override
	protected Boolean doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		String url = params[0];
		Log.e("GCM", url);
		JSONParser parser = new JSONParser();
		Boolean result = parser.makeRequest(url);
		return result;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if(!result)Log.e("GCM", "error sending data to server");
		else Log.e("GCM", "success");
	}
}
