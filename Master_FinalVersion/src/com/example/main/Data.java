package com.example.main;


public class Data {
	public static final int port_room_master_device = 6666 ;
	public static final int port_room_master_monitor = 8888;
	public static int port_app_login 		   = 8000;
	public static int port_app_init 		   = 9000;
	public static int port_app_monitor		   = 9001;
	
	
	
	//application ports
	
	public static final int port_AppToMasterUserLogin 		= 7777;
	public static final int port_MasterToAppInitThread 		= 1111;
	public static final int port_MasterToAppCloseThread 	= 2020;
	public static final int port_AppToMasterPacketReceiver 	= 2222;
	public static final int port_udpAppMonitorThread 		= 12345;
	
	
	public static String GCM_REG_ID = "";
	
	
	public static final int CMD_22 = 22;
	public static final int CMD_23 = 23;
	public static final int CMD_08 = 8;
	public static final int CMD_10 = 10;
	public static final int CMD_11 = 11;
	public static final int CMD_12 = 12;
	public static final int CMD_13 = 13;
	public static final int CMD_14 = 14;
    
}
