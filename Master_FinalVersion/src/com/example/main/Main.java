package com.example.main;

import static com.example.main.CommonUtilities.EXTRA_MESSAGE;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;

import com.example.dataformat.DataFormat;
import com.mahfuz.active.ActiveRoomList;
import com.mahfuz.connection.DataHandler;
import com.mahfuz.database.DatabaseHelper;
import com.mahfuz.database.OutdoorDatabaseHelper;
import com.mahfuz.outdoor.connection.URL;
import com.mahfuz.prev.app.AppToMasterPacketReceiver;
import com.mahfuz.prev.app.AppToMasterUserLoginThread;
import com.mahfuz.prev.app.MasterToAppCloseThread;
import com.mahfuz.prev.app.MasterToAppInitThread;
import com.mahfuz.prev.app.udpAppMonitorThread;
import com.mahfuz.prev.app.udpMasterAlive;
import com.mahfuz.room.Device;
import com.mahfuz.room.Room;
import com.mahfuz.threads.RoomToMasterInitThread;
import com.mahfuz.threads.RoomToMasterMonitorThread;
import com.mahfuz.threads.UdpRoomThread;
import com.mahfuz.ui.Activity_IndoorOutdoor;
import com.mahfuz.ui.updateholder.uiUpdate;

public class Main extends Activity {

	EditText ed_login_user_name,ed_login_user_pass ;
	Button bt_login ;
	CheckBox checkBox ;
	AlertDialogManager alert ;
	public static  Context main ;
	public static String MASTER_IP ;

	public static DatabaseHelper db;
	public static OutdoorDatabaseHelper odb ;
	public static ActiveRoomList active_room_list ;
	public static uiUpdate uiupdate ;

	public static List<String> active_app_list ;

	public static Q queue ;
	final static String TAG = "Main";

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_login);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title_bar);


		///////////////////////////////////////////////////////////////////////////////////
		main = Main.this;

		MASTER_IP = Utils.getIPAddress(true);

		alert = new AlertDialogManager();

		db  = new DatabaseHelper(getApplicationContext());
		odb = new OutdoorDatabaseHelper(getApplicationContext());

		active_room_list = new ActiveRoomList();

		active_app_list = new ArrayList<String>();

		queue = new Q();
		
		uiupdate = new uiUpdate();

		// start of threads
		/*
		 * Thread Initializations
		 */
		new RoomToMasterMonitorThread();	//monitor the room update
		new RoomToMasterInitThread();		//room add thread
		new UdpRoomThread();				//room alive thread


		/*
		 * Application threads
		 */
		new AppToMasterUserLoginThread();
		new AppToMasterPacketReceiver(Main.this);	//monitors the change from user application
		new udpMasterAlive();						//sends broadcast message at port:54321
		new udpAppMonitorThread();					//receive and send master ip to user application
		new MasterToAppCloseThread();				//remove a IP from active user application list
		new MasterToAppInitThread();				//write room controller




		///////////////////////// Initialize ui components ////////////////////////////////
		this.ed_login_user_name = (EditText)findViewById(R.id.ed_login_name);
		this.ed_login_user_pass = (EditText)findViewById(R.id.ed_login_pass);
		this.bt_login 			= (Button)findViewById(R.id.bt_login_login);
		this.checkBox			= (CheckBox)findViewById(R.id.checkbox_login_show_pass);

		this.ed_login_user_name.setHint("Admin username");
		this.ed_login_user_pass.setHint("Password");

		////////////////////////////////////////////////////////////////////////

		checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked) {
					ed_login_user_pass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
				} else {
					ed_login_user_pass.setInputType(129);
				}
			}
		});
		/////////////////////////////////////////////////////////////////////////////

		bt_login.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(ed_login_user_name.getText().toString().trim().length() >0 && ed_login_user_pass.getText().toString().trim().length()>0)
				{
					String name = ed_login_user_name.getText().toString().trim();
					String pass = ed_login_user_pass.getText().toString().trim();

					if(name.equals(getUsername()) && pass.equals(getPassword()))
					{
						//start a indoor/outdoor activity 
						Intent it = new Intent(Main.this,Activity_IndoorOutdoor.class);
						startActivity(it);

					}
					else
					{
						//show fail alert dialog
						alert.showAlertDialog(Main.this, "Sign in Failed!", "Incorrect username or password!", false);
					}
				}
				else
				{
					alert.showAlertDialog(Main.this, "Information missing!", "Username or password is empty", false);
				}
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		this.ed_login_user_pass.setText("");
	}
	/********************** Preference *********************************/

	public static String GetSetGcmIRegId(String regid)
	{
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(main);
		String id = pref.getString("GCM_REG_ID", "");
		if(id.equals(""))
		{
			SharedPreferences.Editor editor = pref.edit();
			editor.putString("GCM_REG_ID", regid);
			id = regid;
			editor.commit();
		}
		return id;
	}
	public static String getUsername()
	{
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(main);
		String name = pref.getString("NAME", "");
		if(name.equals(""))
		{
			SharedPreferences.Editor editor = pref.edit();
			editor.putString("NAME", "admin");
			name = "admin";
			editor.commit();
		}

		return name;

	}
	public String getPassword()
	{
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(main);
		String pass = pref.getString("PASS", "");
		if(pass.equals(""))
		{
			SharedPreferences.Editor editor = pref.edit();
			editor.putString("PASS", "admin");
			pass = "admin";
			editor.commit();
		}

		return pass;
	}
	public static String getSetMasterId(String id)
	{
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(main);
		String pass = pref.getString("MASTER_ID", "00000");
		if(pass.equals("00000"))
		{
			SharedPreferences.Editor editor = pref.edit();
			editor.putString("MASTER_ID", id);
			editor.commit();
		}
		return id;
	}
	public static int getsetInternetSetup(int com)
	{
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(main);
		int id = pref.getInt("INTERNET_SETUP",0);
		if(id==0)
		{
			SharedPreferences.Editor editor = pref.edit();
			editor.putInt("INTERNET_SETUP", com);
			editor.commit();
			id = com ;
		}
		else if(id==-1 && com==1)
		{
			SharedPreferences.Editor editor = pref.edit();
			editor.putInt("INTERNET_SETUP", com);
			editor.commit();
			id = 1 ;
		}
		else if(id==-1 && com==0)
		{
			SharedPreferences.Editor editor = pref.edit();
			editor.putInt("INTERNET_SETUP", com);
			editor.commit();
			id = 0 ;
		}
		return id;
	}
	public static class MasterBroadCastReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			
		   if( intent.getAction().equalsIgnoreCase("app"))
			{
				final String packet = intent.getStringExtra("DATAIN"); 
				Log.d(TAG,packet);


				new Thread(new Runnable() {

					@Override
					public void run() 
					{
						//process packet from application
						StringTokenizer token = new StringTokenizer(packet,",");
						ArrayList<String> tok = new ArrayList<String>();

						while(token.hasMoreElements())
						{
							tok.add(token.nextToken());

						}

						if(tok.get(0).equals(DataFormat.VERSION))
						{
							if(tok.get(1).equals(DataFormat.MASTER_ID))
							{
								if(tok.get(2).equals(DataFormat.GEN_APP))
								{

									if(tok.get(3).equals(DataFormat.CMD))
									{
										String com_id = tok.get(4);

										if(com_id.equals(DataFormat.COMMAND_ID_001))
										{
											//user login
											String user_id = tok.get(5);
											String user_ps = tok.get(6);
											Boolean sucess = false ;

											//check database and return details to applications

											/*
											for(User u : MainActivity.user_list.getUser_list())
											{
												if(u.getName().equals(user_id) && u.getPassword().equals(user_ps))
												{
													// lgoin successfull
													sucess = true ;
													break;
												}
											}

											String muri_kha = DataFormat.get_cmd_oo1(user_id,(sucess)?"Y":"N");
											Notification.NotifyAll(muri_kha);
											 */

										}
										else if(com_id.equals(DataFormat.COMMAND_ID_002))
										{
											//give the user room details.

										}
										else if(com_id.equals(DataFormat.COMMAND_ID_003))
										{
											//a device status change command from app

											String user_id = tok.get(5);
											String room_id = tok.get(6);
											String device_id = tok.get(7);
											String onf = tok.get(8);
											String other=tok.get(9);

											//1.get room details from database.

											//2.send data to room

											List<com.mahfuz.room.Room> room_list = Main.db.getAllRooms();

											for(com.mahfuz.room.Room temp_room : room_list)
											{
												if(temp_room.getId() == Integer.parseInt(room_id))
												{
													String room_ip = temp_room.getIp();

													String temp_rid = "0000" + room_id;
													temp_rid = temp_rid.substring(temp_rid.length()-4, temp_rid.length());

													String temp_did = "0000" + device_id;
													temp_did = temp_did.substring(temp_did.length()-4, temp_did.length());


													String pac = "A" + temp_rid + temp_did + onf + other;
													Log.d(TAG,pac);
													DataHandler.sendData(pac, room_ip, 2000);
												}
											}

											/*
											for(User u:MainActivity.user_list.getUser_list())
											{
												if(u.getUserId().equals(user_id))
												{
													for(String r: u.getRoom_id())
													{
														Room rr = MainActivity.controller.getRoomBy_ID(room_id);
														if(rr!=null)
														{
															String pac = "A" + room_id + device_id + onf + other;
															Log.d(TAG,pac);
															String ip = rr.getIp();
															DataHandler.sendData(pac, ip, 2000);
														}
													}
												}
											}
											 */
										}
										else if(com_id==DataFormat.COMMAND_ID_004)
										{
											//app asks for master id

										}

									}
									else if(tok.get(3).equals(DataFormat.GET))
									{
										// app ask for room device status type 
									}
								}
							}
						}

					}
				}).start();


			}
			/*
			 * packet from room
			 */

			else if(intent.getAction().equalsIgnoreCase("room"))
			{
				final String packet = intent.getStringExtra("DATAIN");
				Log.d(TAG,packet);

				new Thread(new Runnable() 
				{

					@Override
					public void run() 
					{
						// process the packet from room
						if(packet.startsWith("*HELLO*"))
						{
							String data = packet.substring(7);
							String room_id , device_id, on_off , other ;
							if(data.startsWith("R") && data.length()>12)
							{

								room_id = data.substring(1, 5);
								device_id = data.substring(5, 9);
								on_off = data.substring(9, 10);
								other = data.substring(10,13);

								int i_device_id = Integer.parseInt(device_id);
								int i_room_id   = Integer.parseInt(room_id);
								int i_on_off 	= Integer.parseInt(on_off);

								Device d = new Device();
								d.setId(i_device_id);
								d.setOnf(i_on_off);
								d.setRoomid(i_room_id);
								d.setOther(other);


								//Main.db.UpdateDeveiceInfor(d);
								//Room r = MainActivity.controller.getRoomBy_ID(room_id);

								if(Main.db.UpdateDeveiceInfor(d))
								{

									// create a data packet here :
									String packet = DataFormat.get_cmd_003(String.valueOf(d.getRoomid()), String.valueOf(d.getId()), on_off, other);
									// notify the all application here

									//Notification.NotifyAll("C2" + packet);
									Main.uiupdate.update_device_list("",false);
									Main.uiupdate.update_room_list("",false);
									

									for(String ip:active_app_list){
										Log.e(TAG,"Sending notification to app");
										DataHandler.sendData(packet, ip, 2015);
									}
									Log.e(TAG,"database updated!");

								}
							}
						} //end of packet starts with
					}
				}).start(); //end of thread

			}
			////////////////// from gcm message ////////////////////////////////////////////////////////////
			else if(intent.getAction().equalsIgnoreCase(CommonUtilities.DISPLAY_MESSAGE_ACTION))
			{
				String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
				//   mDisplay.append(newMessage + "\n");
				//   Toast.makeText(getApplicationContext(), newMessage, Toast.LENGTH_LONG).show();

				//checking goes here for gcm packet
				//Log.e("ID",newMessage.substring(26, 28));
				Log.e("GCM",newMessage);

				String command_id = newMessage.substring(26, 28);
				
				
				if(command_id.equals("22"))
				{

					if(newMessage.substring(29).equals("1"))
					{
						//generateNotification(context, newMessage,1);
						new myNotification(main).showNotification(newMessage, Data.CMD_22,true);
					}
					else
					{
						new myNotification(main).showNotification(newMessage, Data.CMD_22,false);
					}

				}
				else if(command_id.equals("23"))
				{
					if(newMessage.substring(29).equals("1"))
					{
						//generateNotification(context, newMessage,1);
						new myNotification(main).showNotification(newMessage, Data.CMD_23,true);
					}
					else
					{
						new myNotification(main).showNotification(newMessage, Data.CMD_23,false);
					}
				}
				else if(command_id.equals("08"))
				{
					String header="",data="00000",url;

					StringTokenizer token = new StringTokenizer(newMessage,"~");
					if(token.hasMoreTokens())header = token.nextToken();
					if(token.hasMoreTokens())data = token.nextToken();

					Log.e("GCM", "Master id = " + data);

					String result = getSetMasterId(data);

					if(result.equals("00000"))
					{
						//error . send ack 0
						url = URL.getAckURL(newMessage, false);

					}
					else
					{
						// ok send ack 1
						url = URL.getAckURL(newMessage, true);
					}
					new SendDataToServerInBackGround().execute(url,null,null);				
				}
				else if(command_id.equals("10"))
				{
					String header="",data="00000",url;

					StringTokenizer token = new StringTokenizer(newMessage,"~");
					if(token.hasMoreTokens())header = token.nextToken();
					if(token.hasMoreTokens())data = token.nextToken();

					Log.e("GCM", "Master id = " + data);

					url = URL.getAckURL(newMessage, true);

					//String result = getSetMasterId(data);

					new SendDataToServerInBackGround().execute(url,null,null);				
				}
				else if(command_id.equals("11"))
				{
					String header="",data="00000",url;
					Boolean done = false ;

					StringTokenizer token = new StringTokenizer(newMessage,"~");
					if(token.hasMoreTokens())header = token.nextToken();
					if(token.hasMoreTokens())data = token.nextToken();

					Log.e("GCM", "DATA=" + data);


					// token the data packet
					if(!data.equals("00000"))
					{
						StringTokenizer tkn = new StringTokenizer(data, "|");
						try
						{
							while(tkn.hasMoreTokens())
							{

								int id 		= Integer.parseInt(tkn.nextToken());
								String name = tkn.nextToken();
								int active 	= Integer.parseInt(tkn.nextToken());
								done = (odb.insertType(id, name, active)>=0)?true:false;

							}

						}
						catch(Exception e)
						{
							done = false ;
						}
					}
					url = URL.getAckURL(newMessage, done);
					new SendDataToServerInBackGround().execute(url,null,null);

				}
				else if(command_id.equals("12"))
				{
					String header="",data="00000",url;
					Boolean done = false ;

					StringTokenizer token = new StringTokenizer(newMessage,"~");
					if(token.hasMoreTokens())header = token.nextToken();
					if(token.hasMoreTokens())data = token.nextToken();

					Log.e("GCM", "DATA=" + data);


					// token the data packet
					if(!data.equals("00000"))
					{
						StringTokenizer tkn = new StringTokenizer(data, "|");
						try
						{
							while(tkn.hasMoreTokens())
							{

								int id 		= Integer.parseInt(tkn.nextToken());
								String name = tkn.nextToken();
								done = (odb.insertProperty(id, name)>=0)?true:false;

							}

						}
						catch(Exception e)
						{
							done = false ;
						}
					}
					url = URL.getAckURL(newMessage, done);
					new SendDataToServerInBackGround().execute(url,null,null);

					//url = URL.getAckURL(newMessage, true);

					//String result = getSetMasterId(data);

					new SendDataToServerInBackGround().execute(url,null,null);				
				}
				else if(command_id.equals("13"))
				{
					String header="",data="00000",url;
					Boolean done = false ;

					StringTokenizer token = new StringTokenizer(newMessage,"~");
					if(token.hasMoreTokens())header = token.nextToken();
					if(token.hasMoreTokens())data = token.nextToken();

					Log.e("GCM", "DATA=" + data);


					// token the data packet
					if(!data.equals("00000"))
					{
						StringTokenizer tkn = new StringTokenizer(data, "|");
						try
						{
							while(tkn.hasMoreTokens())
							{

								int tid 		= Integer.parseInt(tkn.nextToken());
								int pid 		= Integer.parseInt(tkn.nextToken());
								done = (odb.insertTypeProperty(tid, pid)>=0)?true:false;

							}

						}
						catch(Exception e)
						{
							done = false ;
						}
					}
					url = URL.getAckURL(newMessage, done);
					new SendDataToServerInBackGround().execute(url,null,null);				
				}
				else if(command_id.equals("14"))
				{

				}
				else if(command_id.equals("15"))
				{

				}
				else if(command_id.equals("16"))
				{

				}
				else if(command_id.equals("17"))
				{
					//save room 
					if(newMessage.subSequence(4, 7).equals("ACK"))
					{
						// after sending the save room the server gives a ACK
						// so now start sending the device status. 
						queue.notify("17");
					}
					
				}
				else if(command_id.equals("18"))
				{
					if(newMessage.subSequence(4, 7).equals("ACK"))
					{
						// after sending the save room the server gives a ACK
						// so now start sending the device status. 
						queue.notify("18");
					}
				}
				else if(command_id.equals("19"))
				{

				}
				else if(command_id.equals("20"))
				{

				}
				else if(command_id.equals("21"))
				{

				}
				else if(command_id.equals("22"))
				{

				}
				else if(command_id.equals("23"))
				{

				}
				else if(command_id.equals("24"))
				{
					String header="",data="00000",url;
					Boolean done = false ;

					StringTokenizer token = new StringTokenizer(newMessage,"~");
					if(token.hasMoreTokens())header = token.nextToken();
					if(token.hasMoreTokens())data = token.nextToken();

					Log.e("GCM", "DATA=" + data);


					// token the data packet
					
					if(data.equals("00000"))
					{
						done = false ;
					}
					else if(data.equals("1"))
					{
						int id = getsetInternetSetup(1);
						done = (id==1)?true:false;
						
						
					}
					else if(data.equals("0"))
					{
						done = false ;
						getsetInternetSetup(0);
					}
					url = URL.getAckURL(newMessage, done);
					new SendDataToServerInBackGround().execute(url,null,null);
					
					// send the list of room properties.
					
					//List<Room> roomlist = new ArrayList<Room>();
					String saveroomurl = URL.getSaveRoomURL(1,"","",0);
					queue.add(saveroomurl, "17");
					
					
					/*
					roomlist = Main.db.getAllRooms();
					
					for(Room rm : roomlist)
					{
						String saveroomurl = URL.getSaveRoomURL(rm.getId(),rm.getName(),rm.getIp(),rm.getActive());
						//new SendDataToServerInBackGround().execute(saveroomurl,null,null);
						queue.add(saveroomurl, "17");
						List<Device> devicelist = Main.db.getDeviceByRoom(rm.getId());
						{
							for(Device d:devicelist)
							{
								String savedeviceurl = URL.getSaveDeviceURL(d.getId(),d.getName());
								//new SendDataToServerInBackGround().execute(savedeviceurl,null,null);
								queue.add(savedeviceurl, "18");
							}						
						}
					}
					*/
					//new  saveroomurl = URL.getSaveRoomURL();
					//new SendDataToServerInBackGround().execute(params,null,null);
				}
			}
		}
	}
}
