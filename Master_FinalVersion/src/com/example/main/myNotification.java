package com.example.main;

import com.mahfuz.ui.Activity_Outdoor_Initialization;
import com.mahfuz.ui.Activity_Outdoor_Login;
import com.mahfuz.ui.Activity_outdoor_codeInput;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.example.main.Data;
public class myNotification {
	
	private static final String TAG = "GCM";
	Context context ;
	
	public myNotification(Context con) 
	{
		// TODO Auto-generated constructor stub
		this.context = con;
	}
	@SuppressWarnings("deprecation")
	void showNotification(String message,int key,boolean success)
	{
		Intent notificationIntent ; 
		switch (key) 
		{
			case Data.CMD_22:
			{
				notificationIntent = (success)?new Intent(context, Activity_outdoor_codeInput.class):new Intent(context, Activity_Outdoor_Initialization.class);
				message = (success)?"Check your mail for activation code":"Request fail.check your email address";
			}
			break;
			
			case Data.CMD_23:
			{
				notificationIntent = (success)?new Intent(context, Activity_Outdoor_Login.class):new Intent(context, Activity_outdoor_codeInput.class);
				message = (success)?"Your Home is activated":"Request fail.check your code from email";
			}
			default:
			{
				notificationIntent = new Intent(context, DemoActivity.class);
			}
			break;
		}

		// Intent notificationIntent = new (context, DemoActivity.class);
		// set intent so it does not start a new activity
		int icon = R.drawable.ic_home;
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, message, when);
		String title = context.getString(R.string.app_name);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
		notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(0, notification);

		Log.d(TAG, message); 
	}
}
