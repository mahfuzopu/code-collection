/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.main;

import static com.example.main.CommonUtilities.SENDER_ID;
import static com.example.main.CommonUtilities.SERVER_URL;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.mahfuz.ui.Activity_Outdoor_Login;
import com.mahfuz.ui.Activity_outdoor_codeInput;

/**
 * Main UI for the demo app.
 */


public class DemoActivity extends Activity  {
	SharedPreferences pref ;
	public final static String TAG = "DemoActivity";

	TextView mDisplay;
	AsyncTask<Void, Void, Void> mRegisterTask;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		checkNotNull(SERVER_URL, "SERVER_URL");
		checkNotNull(SENDER_ID, "SENDER_ID");
		// Make sure the device has the proper dependencies.
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		//   setContentView(R.layout.main);
		//    mDisplay = (TextView) findViewById(R.id.display);
		//registerReceiver(mHandleMessageReceiver,new IntentFilter(DISPLAY_MESSAGE_ACTION));

		//check internet connection

		ConnectionDetector con = new ConnectionDetector(DemoActivity.this);

		if(!con.isConnectingToInternet()){

			new AlertDialog.Builder(this).setIcon(R.drawable.fail)
			.setTitle("Error")
			.setMessage("No active internet connection found!")
			.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					finish();    
				}
			}).setCancelable(false)
			.show();
		}
		else
		{

			final String regId = GCMRegistrar.getRegistrationId(getApplicationContext());

			Log.d(TAG, "gcm id is = " + regId);
			
			String tregid = Main.GetSetGcmIRegId(regId);

			Log.d(TAG, "gcm reg id from pref = " + tregid);

			
			if(tregid.equals(""))
			{
				new AsyncTask<Void, Void, Void>()
				{

					@Override
					protected Void doInBackground(Void... params) {
						// TODO Auto-generated method stub
						GCMRegistrar.register(getApplicationContext(), SENDER_ID);
						return null;
					}

					@Override
					protected void onPostExecute(Void result) {
						// TODO Auto-generated method stub
						super.onPostExecute(result);
						finish();
						Intent it = new Intent(DemoActivity.this,Activity_Outdoor_Login.class);
						startActivity(it);
					}
					
					
				}.execute(null,null,null);
				
			}
			else
			{

				finish();
				Intent it = new Intent(this,Activity_Outdoor_Login.class);
				startActivity(it);

			}


			/*
			if (regId.equals("")) {
				// Automatically registers application on startup.
				GCMRegistrar.register(this, SENDER_ID);

				new AlertDialog.Builder(this).setIcon(R.drawable.fail)
				.setTitle("Error")
				.setMessage("Your device is not ready yet.please try again later.")
				.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						finish();    
					}
				}).setCancelable(false)
				.show();

			} else {
				// Device is already registered on GCM, check server.

				Data.GCM_REG_ID = regId;
				Log.d(TAG, "gcm is registered! and its id is = " + regId);

				Main.GetSetGcmIRegId(regId);

				if (GCMRegistrar.isRegisteredOnServer(this)) {
					// Skips registration.
					//   mDisplay.append(getString(R.string.already_registered) + "\n");
				} else {
					// Try to register again, but not in the UI thread.
					// It's also necessary to cancel the thread onDestroy(),
					// hence the use of AsyncTask instead of a raw thread.
					//   final Context context = this;
					mRegisterTask = new AsyncTask<Void, Void, Void>() {

						@Override
						protected Void doInBackground(Void... params) {
							//     boolean registered =
							//             ServerUtilities.register(context, regId);
							// At this point all attempts to register with the app
							// server failed, so we need to unregister the device
							// from GCM - the app will try to register again when
							// it is restarted. Note that GCM will send an
							// unregistered callback upon completion, but
							// GCMIntentService.onUnregistered() will ignore it.

							Log.d(TAG, "within the back ground process!");

							//    if (!registered) {
							//        GCMRegistrar.unregister(context);
							//    }
							return null;
						}

						@Override
						protected void onPostExecute(Void result) {
							mRegisterTask = null;
						}

					};
					mRegisterTask.execute(null, null, null);
				}
			}
			finish();

			// start the login activity here 

			Intent it = new Intent(this,Activity_Outdoor_Login.class);
			startActivity(it);
			finish();
			 */
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//    MenuInflater inflater = getMenuInflater();
		//   inflater.inflate(R.menu.options_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {

		/*
		 * Typically, an application registers automatically, so options
		 * below are disabled. Uncomment them if you want to manually
		 * register or unregister the device (you will also need to
		 * uncomment the equivalent options on options_menu.xml).
		 */
		/*
            case R.id.options_register:
                GCMRegistrar.register(this, SENDER_ID);
                return true;
            case R.id.options_unregister:
                GCMRegistrar.unregister(this);
                return true;
		 */
		//      case R.id.options_clear:
		//            mDisplay.setText(null);
		//            return true;
		//        case R.id.options_exit:
		//            finish();
		//            return true;
		//        default:
		//           return super.onOptionsItemSelected(item);
		}
		return true;
	}

	@Override
	protected void onDestroy() {
		//if (mRegisterTask != null) {
		//	mRegisterTask.cancel(true);
		//}
	//	unregisterReceiver(mHandleMessageReceiver);
		GCMRegistrar.onDestroy(getApplicationContext());
		super.onDestroy();
	}

	private void checkNotNull(Object reference, String name) {
		if (reference == null) {
			throw new NullPointerException(
					getString(R.string.error_config, name));
		}
	}

	/*
	private final BroadcastReceiver mHandleMessageReceiver =
			new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			if(intent.getAction().equalsIgnoreCase(CommonUtilities.DISPLAY_MESSAGE_ACTION))
			{
				String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
				//   mDisplay.append(newMessage + "\n");
				//   Toast.makeText(getApplicationContext(), newMessage, Toast.LENGTH_LONG).show();

				//checking goes here for gcm packet
				//Log.e("ID",newMessage.substring(26, 28));
				Log.e("GCM",newMessage);

				if(newMessage.substring(26, 28).equals("22"))
				{

					if(newMessage.substring(29).equals("1"))
					{
						generateNotification(context, newMessage,1);
					}

				}
				else
				{
					Toast.makeText(getApplicationContext(), newMessage, Toast.LENGTH_LONG).show();
				}
			}

		}
	};
	*/

	@SuppressWarnings({ "deprecation", "unused" })
	private static void generateNotification(Context context, String message,int key) {

		//Toast.makeText(context, message, Toast.LENGTH_LONG).show();

		int icon = R.drawable.ic_app_title;
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager)
				context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, message, when);
		String title = context.getString(R.string.app_name);

		Intent notificationIntent = new Intent(context, DemoActivity.class);

		switch (key) {
		case 1:
			notificationIntent = new Intent(context, Activity_outdoor_codeInput.class);
			break;

		default:
			break;
		}

		// Intent notificationIntent = new (context, DemoActivity.class);
		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
				Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
		notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(0, notification);

		Log.d(TAG, message);      
	}
}