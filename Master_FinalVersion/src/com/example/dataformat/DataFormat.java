package com.example.dataformat;

public class DataFormat {
	public static String VERSION = "01";
	public static String MASTER_ID="0000";
	public static String USER_ID = "0000";
	public static String GEN_APP = "APP";
	public static String GEN_MST = "MST";
	public static String GEN_SER = "SER";
	public static String CMD = "CMD";
	public static String GET = "GET";
	public static String ACK = "ACK";
	
	public static String COMMAND_ID_001 = "001";
	public static String COMMAND_ID_002 = "002";
	public static String COMMAND_ID_003 = "003";
	public static String COMMAND_ID_004 = "004";
	public static String COMMAND_ID_005 = "005";
	
	public static String getHeader(String type)
	{
		return VERSION + "," + MASTER_ID + "," + GEN_MST + "," + type ;
	}
	public static String get_cmd_003(String room_id,String device_id,String onf,String other)
	{
		return getHeader(DataFormat.CMD) + "," + DataFormat.COMMAND_ID_003 + "," + room_id +","+ device_id +","+ onf +","+ other;
	}
	public static String get_cmd_oo1(String user_id, String yn)
	{
		return getHeader(DataFormat.CMD) + "," + DataFormat.COMMAND_ID_001 +","+ user_id+","+ yn;
	}
	public static String get_cmd_002(String rmdetails)
	{
		return getHeader(DataFormat.CMD) + "," + DataFormat.COMMAND_ID_002 +","+ rmdetails;
	}
	public static String get_cmd_003(String room_id,String AI)
	{
		return getHeader(DataFormat.CMD) + "," + DataFormat.COMMAND_ID_003 +","+ room_id +"," + AI;
	}
}
