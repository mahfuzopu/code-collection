package com.example.users;

import java.util.ArrayList;

public class User {
	String name ;
	String password ;
	int user_id ;
	int active ;
	
	public static int count_user= 0 ;
	
	ArrayList<String> assigned_room = new ArrayList<String>();
	
	public User() {
		// TODO Auto-generated constructor stub
		assigned_room = new ArrayList<String>();
		//String t_id = "00000" + count_user ;
		//setUserId(t_id.substring(t_id.length()-4, t_id.length()));
		count_user++;
		//Log.d("User","A new User Created with id:" + this.getUserId());
	}

	public int getUserId() {
		return this.user_id;
	}
	public void setUserId(int id){
		this.user_id = id;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ArrayList<String> getRoom_id() {
		return assigned_room;
	}
	public void AssignRoom(String room_id){
		assigned_room.add(room_id);
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}
	
}
