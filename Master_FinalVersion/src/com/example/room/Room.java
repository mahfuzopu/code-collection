/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.room;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author mahfuz
 */
public class Room implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String id ;
    String name;
    String ip ;
    ArrayList<Device> device_list ;
    Boolean isActive ; 
    
    public Room(){
        this.device_list = new ArrayList<Device>();
    }
    public void add_device(Device d){
        this.device_list.add(d);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public ArrayList<Device> getDevice_list() {
        return device_list;
    }
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}
