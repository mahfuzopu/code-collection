/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.room;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Mahfuz
 */

public class RoomController implements Serializable{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ArrayList<Room> room_list ;

    public RoomController() {
        this.room_list = new ArrayList<Room>();
    }

    public ArrayList<Room> getRoom_list() {
        return room_list;
    }
    
    public void add_room(Room r){
        this.room_list.add(r);
    }
    public void remove_room(Room r){
        this.room_list.remove(r);
    }
    public String getRoomId_by_Ip(String ip){
        for(int i = 0 ; i < room_list.size();i++){
            if(room_list.get(i).getIp().equals(ip)){
                return room_list.get(i).getId();
            }
        }
        return null;
    }
    public String getRoomIp_by_Id(String id){
        for(int i = 0 ; i < room_list.size();i++){
            if(room_list.get(i).getId().equals(id)){
                return room_list.get(i).getIp();
            }
        }
        return null;
    }
    public String getName_by_ID(String id){
        for(int i = 0 ; i < room_list.size();i++){
            if(room_list.get(i).getId().equals(id)){
                return room_list.get(i).getName();
            }
        }
        return null;
        
    }
    public String getName_by_Ip(String ip){
        for(int i = 0 ; i < room_list.size();i++){
            if(room_list.get(i).getIp().equals(ip)){
                return room_list.get(i).getIp();
            }
        }
        return null;
        
    }
    public Room getRoomBy_Ip(String ip){
    	for(int i = 0 ; i < room_list.size() ; i++){
    		if(room_list.get(i).getIp().equals(ip)){
    			return room_list.get(i);
    		}
    	}
		return null;
    }
    public Room getRoomBy_ID(String id){
    	for(int i = 0 ; i < room_list.size() ; i++){
    		if(room_list.get(i).getId().equals(id)){
    			return room_list.get(i);
    		}
    	}
		return null;
    }
}