/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.room;

import java.io.Serializable;

/**
 *
 * @author mahfuz
 */

public class Device implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String id ;
    private String name;
    private String type;
    private String other;
    private String on ;

    public Device(){
    
    }
    public String geton() {
        return on;
    }
    public void setOn(String on) {
        this.on = on;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }
}
