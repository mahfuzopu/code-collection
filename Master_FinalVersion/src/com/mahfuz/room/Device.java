package com.mahfuz.room;

public class Device {

	int id ;
	String name ;
	String type ;
	String other;
	int onf ; //0 for OFF , 1 for ON
	int room_id ;
	
	public Device()
	{
		
	}

	public void setRoomid(int r)
	{
		this.room_id = r ;
	}
	public int getRoomid()
	{
		return this.room_id;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public int getOnf() {
		return onf;
	}

	public void setOnf(int onf) {
		this.onf = onf;
	}
}
