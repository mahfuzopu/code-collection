package com.mahfuz.connection;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

import android.util.Log;


public class DataHandler
{
	final static String tag = "DataHandler";
		
	public static void sendData(final String msg, String ip, final int port)
	{
		final String targetIP = ip;
		new Thread(new Runnable()
		{
			BufferedWriter out = null;
			Socket clientSocket = null;
			public void run()
			{
				try
				{					
					clientSocket = new Socket(targetIP, port);
					out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
					try
					{
						out.write(msg);
					}
					catch (Exception e)
					{
						Log.d("CMD send Exception", e.getMessage());
					}
					finally
					{
						out.flush();
						out.close();
					}	
					Log.d(tag, "sent");
				}
				catch (Exception ex)
				{
					Log.d(tag, ex.getMessage());
				}
				finally
				{
					if(clientSocket!=null)
					{
						if (clientSocket.isClosed() != true)
						{
							try
							{
								clientSocket.close();
							}
							catch (IOException e)
							{
								Log.d(tag, e.getMessage());
							}
						}
					}
				}

			}
		}).start();
	}
}
