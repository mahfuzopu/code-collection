package com.mahfuz.outdoor.connection;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.example.main.Main;

public class URL {
	public final static String SERVER = "http://dev.sinepulse.com/smarthome/service/dev/DataAPI.svc/data/";
	public final static String VERSION = "0002";

	public static String getLoginURL(String name,String pass)
	{
		return SERVER + "login?user="+name+"&pass="+pass;
	}
	public static String getInitializationURL(String mac,String gcmregid,String email)
	{
		//APA91bGStznzeiMJPE6dqtOucK6pKiTMTwIQ6YFZy1Ey9_RW1HEMAPEnydnMNnSXZ0-VYfkpiD8ukiJ1XBA7K2Vsos6zaztoRxJemi2t1U9GJGhQE6nc4Fy0R2EZh407kp3A1irwVLH7UOlYMibxiIQ8Qblyuc6bfQ
		return SERVER + "get/"+VERSION+"INI"+getTimeStamp()+"0000022~"+mac+"~"+email+"~"+gcmregid;
		//return SERVER + "get/0002INI031420141848000000022~001~mahfuj.islam@sinepulse.com~APA91bGStznzeiMJPE6dqtOucK6pKiTMTwIQ6YFZy1Ey9_RW1HEMAPEnydnMNnSXZ0-VYfkpiD8ukiJ1XBA7K2Vsos6zaztoRxJemi2t1U9GJGhQE6nc4Fy0R2EZh407kp3A1irwVLH7UOlYMibxiIQ8Qblyuc6bfQ";
	}
	public static String getHomeActiveURL(String mac,String code)
	{
		return SERVER +"get/"+VERSION+"INI"+getTimeStamp()+"0000023~"+mac+"~"+code;
	}
	public static String getAckURL(String pac,boolean iscorrect)
	{
		String rep = pac.replace("CMD", "ACK");
		String trep = rep.substring(0, 28);
		trep+= iscorrect?"~1":"~0";
		return SERVER + "get/"+ trep;
	}
	
	public static String getSaveRoomURL(int i, String string, String string2, int j)
	{ 
		return "http://dev.sinepulse.com/smarthome/service/dev/DataAPI.svc/data/get/0002CMD031420141848000000517~1|Room1|1|Test~1";
		//return SERVER+"get/" + VERSION + "CMD" + getTimeStamp() + Main.getSetMasterId("00000") + "~" ;
	}
	public static String getSaveDeviceURL(int i, String string)
	{
		return "";
		
	}
	public static String getSaveUserURL()
	{
		return "";
	}
	public static String getAssignUserURL()
	{
		return "";
	}
	public static String getDeicePropertyURL()
	{
		return "";
	}
	public static String getDeviceStatusURL()
	{
		return "";
	}
	
	
	
	
	
	
	/*
	 * 
	 * some additional functionalities
	 */
	static String getTimeStamp()
	{
		try 
		{

			SimpleDateFormat dateFormat = new SimpleDateFormat("MMddyyyyhhmmss");
			String currentTimeStamp = dateFormat.format(new Date()); 
			return currentTimeStamp;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();

			return "00000000000000";
		}
	}
}
