package com.mahfuz.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.main.Main;
import com.example.main.R;
import com.mahfuz.room.Room;

public class Activity_Setting_Room extends Activity{

	List<Room> room_list = new ArrayList<Room>();
	ListView lv ;
	RoomListAdapter adpter ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		room_list = Main.db.getAllRooms();
		setContentView(R.layout.activity_setting_room);
		
		
		if(room_list.size()==0)
		{
			Toast.makeText(this, "User List Empty", Toast.LENGTH_SHORT).show();
		}
		lv = (ListView)findViewById(R.id.list_setting_room);
		adpter = new RoomListAdapter();
		lv.setAdapter(adpter);

		lv.setLongClickable(true);
		
		
		 lv.setOnItemClickListener(new OnItemClickListener() {
	            @Override
	            public void onItemClick(AdapterView<?> parent, View view, int position,
	                    long id) {
	                Intent intent = new Intent(Activity_Setting_Room.this, Activity_Setting_Room_Edit.class);
	                //String message = "abc";
	               // intent.putExtra(EXTRA_MESSAGE, message);
	                startActivity(intent);
	            }
	        });
		
	}
	public class RoomListAdapter extends BaseAdapter
	{		
		private static final String TAG = "Activity_Setting_Room";

		public RoomListAdapter()
		{

		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return room_list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convirtView, ViewGroup parent) {
			// TODO Auto-generated method stub
			LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vi = inflater.inflate(R.layout.activity_room_list,parent, false);


			TextView tt = (TextView)vi.findViewById(R.id.tx_room_name);
			TextView tx_active = (TextView)vi.findViewById(R.id.tx_room_active);
			String aa = room_list.get(position).getName();
			Log.d(TAG,"Name:" +aa);
			tt.setText(aa);

			tx_active.setText(room_list.get(position).getActive()==1?"Active":"Inactive");

			return vi;
		}

	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		this.room_list.clear();
		this.room_list = Main.db.getAllRooms();
		adpter.notifyDataSetChanged();
	}

}
