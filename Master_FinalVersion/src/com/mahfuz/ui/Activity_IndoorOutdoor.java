package com.mahfuz.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.example.main.DemoActivity;
import com.example.main.R;

public class Activity_IndoorOutdoor extends Activity {

	Button bt_indoor , bt_outdoor , bt_setting;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_indoor_outdoor);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title_bar_normal);
		
		
		this.bt_indoor 	= (Button)findViewById(R.id.bt_indrrod_outdoor_homecontrol);
		this.bt_outdoor = (Button)findViewById(R.id.indoor_outdoor_bt_internet);
		this.bt_setting = (Button)findViewById(R.id.bt_bar_setting);
		
		
		
		
		
		bt_indoor.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// start indoor normal activity
				
				Intent it = new Intent(Activity_IndoorOutdoor.this,Activity_Indoor_RoomList.class);
				startActivity(it);				
			}
		});
		
		bt_outdoor.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stubte
				Intent it = new Intent(Activity_IndoorOutdoor.this,DemoActivity.class);
				startActivity(it);
				
				
			}
		});	
		
		bt_setting.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// Set setting aqctivity
				Intent it = new Intent(Activity_IndoorOutdoor.this,Activity_Setting.class);
				startActivity(it);
			}
		});	
		
	}
}
