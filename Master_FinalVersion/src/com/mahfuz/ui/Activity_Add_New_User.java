package com.mahfuz.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import com.example.main.AlertDialogManager;
import com.example.main.Main;
import com.example.main.R;
import com.example.users.MultiSelectionSpinner;
import com.example.users.User;
import com.mahfuz.room.Room;

public class Activity_Add_New_User extends Activity{

	protected static final String TAG = "Activity_Add_New_User";
	Button add_user , cancel ;
	EditText user_name,user_pass ;
	MultiSelectionSpinner spinner ;
	List<Room> room_list;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_setting_user_addnewuser);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title_bar_setting);
		
		add_user = (Button)findViewById(R.id.bt_setting_system_change_pass_okay);
		cancel   = (Button)findViewById(R.id.bt_setting_system_change_pass_cancel);
		user_name = (EditText)findViewById(R.id.etx_user_name);
		user_pass = (EditText)findViewById(R.id.etx_user_pass);
		
		room_list = Main.db.getAllRooms();
		
		spinner = (MultiSelectionSpinner)findViewById(R.id.mySpinner1);
		
		List<String>	room_name_list 	= new ArrayList<String>();
		final List<Integer> 	room_id_list 	= new ArrayList<Integer>();
		
		for(Room rr: room_list)
		{
			room_name_list.add(rr.getName());
			room_id_list.add(rr.getId());
		}
		
		if(!room_list.isEmpty())spinner.setItems(room_name_list);
		else 
		{
			String arra[] = {"No Room"};
			spinner.setItems(arra);
		}
		
		add_user.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String name = user_name.getText().toString().trim();
				String pass = user_pass.getText().toString().trim();
				
				if(name.length()>0 && pass.length() >0)
				{
					//add a new user to database
					User u = new User();
					u.setActive(1);
					u.setName(name);
					u.setPassword(pass);
					long id = Main.db.createUser(u);
					u.setUserId((int)id);
					//Main.db.closeDB();
					
					String spi = spinner.getSelectedItemsAsString();
					StringTokenizer tokenToken = new StringTokenizer(spi,",");
					int i = 0 ;
					while (tokenToken.hasMoreElements()) {
						String object =  (String) tokenToken.nextElement();
						//u.AssignRoom(room_id_list.get(i));
						//Toast.makeText(getApplicationContext(), "Assigened rooom id" + list_id.get(i), Toast.LENGTH_SHORT).show();
						Log.e(TAG,"room name =" + object  + " && id = " + room_id_list.get(i));
						if(Main.db.assignUserToRoom(u,room_list.get(i)))
						{
							Log.e(TAG,"user=" + u.getName() +" is assigned to room="+ room_list.get(i).getName() );
						}
						i++;
					}
					
					Log.d(TAG,"A new user created!" + id);
					finish();
				}
				else
				{
					AlertDialogManager alert = new AlertDialogManager();
					alert.showAlertDialog(Activity_Add_New_User.this, "Information missing!", "username or password is empty!",	 false);
				}				
			}
		});
		

		cancel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		
	}
}
