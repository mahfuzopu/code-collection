package com.mahfuz.ui;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Window;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;

import com.example.main.R;

@SuppressWarnings("deprecation")
public class Activity_Setting extends TabActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_setting);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title_bar_setting);
		
		Resources ressources = getResources(); 
		final TabHost tabHost = getTabHost(); 
		

		
		// home tab
		Intent intentStatus = new Intent().setClass(this, Activity_Setting_Room.class);
		TabSpec tabSpecStatus = tabHost
			.newTabSpec("Room Setting")
			.setIndicator("Room", ressources.getDrawable(R.drawable.ic_setting_room))
			.setContent(intentStatus);

		// Reporting tab
		Intent intentReporting = new Intent().setClass(this, Activity_Setting_System.class);
		TabSpec tabSpecReporting = tabHost
			.newTabSpec("System Setting")
			.setIndicator("System", ressources.getDrawable(R.drawable.ic_setting_system))
			.setContent(intentReporting);
		
		// configuration tab
		Intent intentAnalysis = new Intent().setClass(this, Activity_Setting_User.class);
		TabSpec tabSpecAnalysis = tabHost
			.newTabSpec("User Setting")
			.setIndicator("User", ressources.getDrawable(R.drawable.ic_setting_user))
			.setContent(intentAnalysis);

		// add all tabs 
		tabHost.addTab(tabSpecAnalysis);
		tabHost.addTab(tabSpecReporting);
		tabHost.addTab(tabSpecStatus);
		
		//tabHost.addTab(tabSpecBerry);
		
		//set Windows tab as default (zero based)
		
		
		setTabColor(tabHost);
		tabHost.setCurrentTab(0);
		
		tabHost.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String arg0) {
				// TODO Auto-generated method stub
				setTabColor(tabHost);
			}
		});

		
	}
	//Change The Backgournd Color of Tabs
	public void setTabColor(TabHost tabhost) {      

	     for(int i=0;i<tabhost.getTabWidget().getChildCount();i++)  
	            tabhost.getTabWidget().getChildAt(i).setBackgroundColor(Color.LTGRAY); //unselecte
	     tabhost.getTabWidget().getChildAt(tabhost.getCurrentTab()).setBackgroundColor(Color.rgb(26,140, 255)); // selected
	}
	
}
