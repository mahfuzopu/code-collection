package com.mahfuz.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.main.Main;
import com.example.main.R;
import com.mahfuz.outdoor.connection.JSONParser;
import com.mahfuz.outdoor.connection.URL;

public class Activity_Outdoor_Login extends Activity{

	EditText name , pass ;
	Button bt_outdoor_login;
	TextView tx_error ;
	ProgressDialog progress ;
	AsyncTask<Void, Void, Boolean> asyncLogin;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_outdoor_login);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title_bar);
		
		name = (EditText)findViewById(R.id.etx_outdoor_login_email);
		pass = (EditText)findViewById(R.id.etx_outdoor_login_pass);
		tx_error = (TextView)findViewById(R.id.txt_outdoor_login_error);
		
		tx_error.setText("");
		 
		bt_outdoor_login = (Button)findViewById(R.id.bt_outdoor_login);
		
		bt_outdoor_login.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final String uname = name.getText().toString().trim();
				final String upass = pass.getText().toString().trim();
				
				if(uname.equals("") || upass.equals(""))
				{
					
					Toast.makeText(Activity_Outdoor_Login.this, "Name or Password is empty!", Toast.LENGTH_SHORT).show();
				}
				else
				{
					// make a call to server and authenticate
					// back ground process and login request
					asyncLogin = new AsyncTask<Void, Void, Boolean>()
					{
						@Override
						protected void onPreExecute() 
						{
							// TODO Auto-generated method stub
							super.onPreExecute();
							progress = new ProgressDialog(Activity_Outdoor_Login.this);
							progress.setTitle("Please wait");
							progress.setMessage("Signing in...");
							progress.setCancelable(true);
							progress.show();
							

						}	
						@Override
						protected Boolean doInBackground(Void... arg0) 
						{
							// TODO Auto-generated method stub
							JSONParser parser = new JSONParser();
							//return parser.makeLoginRequest("http://dev.sinepulse.com/smarthome/service/dev/DataAPI.svc/data/login?user=companyadmin&pass=companyadmin");
							return parser.makeLoginRequest(URL.getLoginURL(uname, upass));
						}

						@Override
						protected void onPostExecute(Boolean result) 
						{
							// TODO Auto-generated method stub
							super.onPostExecute(result);
							if(result)
							{
								int key = Main.getsetInternetSetup(0);
								
								switch (key) 
								{
									case -1:
										Toast.makeText(Activity_Outdoor_Login.this,"Processing setup..please wait!", Toast.LENGTH_LONG).show();
										break;
									case 1:
										Toast.makeText(Activity_Outdoor_Login.this,"Yor internet control is already active!", Toast.LENGTH_LONG).show();
										break;
										
									default:
										Toast.makeText(Activity_Outdoor_Login.this, "Login sucessfull!", Toast.LENGTH_SHORT).show();
										Intent it = new Intent(Activity_Outdoor_Login.this,Activity_Outdoor_Initialization.class);
										startActivity(it);
										finish();
										break;
								}
							}
							else
							{
								tx_error.setText("Login Error!");
							}
							progress.dismiss();
						}
					};
				}
				asyncLogin.execute(null,null,null);
			}
		});
	}
}
