package com.mahfuz.ui;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.main.Main;
import com.example.main.R;
import com.example.users.User;

public class Activity_Setting_User extends Activity{
	Button bt_adduser ;
	List<User> user_list ;
	ListView lv ;
	UserListAdapter adpter;
	String TAG = "Activity_Setting_User";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting_user);

		user_list = Main.db.getAllUser();

		if(user_list.size()==0)
		{
			Toast.makeText(this, "User List Empty", Toast.LENGTH_SHORT).show();
		}
		lv = (ListView)findViewById(R.id.list_setting_user);
		adpter = new UserListAdapter();
		lv.setAdapter(adpter);

		lv.setLongClickable(true);

		lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> view, View vi,
					final int position, long idd) {
				// TODO Auto-generated method stub
				Log.e("long clicked","pos: " + position);

				AlertDialog dialog ; 
				final CharSequence[] items = { "Delete User"};

				AlertDialog.Builder builder = new AlertDialog.Builder(Activity_Setting_User.this);
				builder.setIcon(R.drawable.ic_action_delete);

				builder.setTitle(user_list.get(position).getName());
				builder.setItems(items, new DialogInterface.OnClickListener(){
					public void onClick(DialogInterface dialog, int pos) {

						switch (pos) {

						case 0:
							User u = user_list.get(position);
							int id = u.getUserId();
							Main.db.DeleteUser(id);
							user_list.clear();
							user_list = Main.db.getAllUser();
							adpter.notifyDataSetChanged();
							Log.d(TAG,"user deleted with id=" + id);

						default:
							break;
						}
					}
				});
				dialog=builder.create();

				dialog.show();



				return false;
			}
		});

		bt_adduser = (Button)findViewById(R.id.bt_setting_user_add_user);
		bt_adduser.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent it = new Intent(Activity_Setting_User.this,Activity_Add_New_User.class);
				startActivity(it);
			}
		});



	}
	public class UserListAdapter extends BaseAdapter
	{		
		public UserListAdapter()
		{

		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return user_list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convirtView, ViewGroup parent) {
			// TODO Auto-generated method stub
			LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vi = inflater.inflate(R.layout.activity_user_list,parent, false);


			TextView tt = (TextView)vi.findViewById(R.id.tx_user_name);
			String aa = user_list.get(position).getName();
			Log.d(TAG,"Name:" +aa);
			tt.setText(aa);


			return vi;
		}

	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		this.user_list.clear();
		this.user_list = Main.db.getAllUser();
		adpter.notifyDataSetChanged();
	}
}
