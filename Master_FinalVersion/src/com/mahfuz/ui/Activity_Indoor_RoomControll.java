package com.mahfuz.ui;


import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.example.main.Main;
import com.example.main.R;
import com.mahfuz.connection.DataHandler;
import com.mahfuz.room.Device;

public class Activity_Indoor_RoomControll extends ListActivity {

	RoomControlAdapter adapter ; 
	final static String TAG = "Activity_Indoor_RoomControll"; 
	int roomid ;
	int fanprogress;
	static int called = 0 ;
	public ArrayList<Device> values ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//setTitle("Available Room List");

		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);

		Main.uiupdate.setDeviceListHolder(Activity_Indoor_RoomControll.this);

		//Main.ui_update.setRoom_list(Activity_Indoor_RoomList.this);

		roomid = getIntent().getIntExtra("roomid", -1);


		adapter = new RoomControlAdapter(Main.db.getDeviceByRoom(roomid));
		setListAdapter(adapter);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title_bar);


	}
	/*
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);

		//values.get(position);

		//Device selectedValue = (Device)getListAdapter().getItem(position);
		//Toast.makeText(this, selectedValue.getType(), Toast.LENGTH_SHORT).show();


		Device selectedValue = values.get(position);

		if(selectedValue.getType().contains("Light")){
			showLightConTrol(selectedValue);
		}
		else if(selectedValue.getType().contains("Fan")){
			showFanControl(selectedValue,"Fan Control","Set Fan Speed" , "Fan Speed");
			//roomcomp[position].setProgress(speed);

		}
		else if(selectedValue.getType().contains("Temp")){
			showFanControl(selectedValue,"Temparature Control","Set Temparature","Temp");
			//roomcomp[position].setProgress(speed);
		}		
		else if(selectedValue.getType().contains("Brightness")){
			showFanControl(selectedValue,"Brightness Control","Set Brightness","Ratio");
			//roomcomp[position].setProgress(speed);

		}
		//String selectedValue = (String) getListAdapter().getItem(position);
		//Toast.makeText(this, selectedValue, Toast.LENGTH_SHORT).show();


		//	Intent it = new Intent(Activity_Indoor_RoomList.this,RoomControl.class);
		//	it.putExtra("room_number",MainActivity.controller.getRoom_list().get(position).getId());	// here position is the room number // will be change according to room propertices
		//	it.putExtra("position",String.valueOf(position));
		//	startActivity(it);

	}
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		Device d = values.get(position);
		
		//Toast.makeText(Activity_Indoor_RoomControll.this, "ID:"+d.getId() + "^^ posiiton:"+position, Toast.LENGTH_SHORT).show();

		Device selectedValue = values.get(position);

		if(selectedValue.getType().contains("Light")){
			showLightConTrol(selectedValue);
		}
		else if(selectedValue.getType().contains("Fan")){
			showFanControl(selectedValue,"Fan Control","Set Fan Speed" , "Fan Speed");
			//roomcomp[position].setProgress(speed);

		}
		else if(selectedValue.getType().contains("Temp")){
			showFanControl(selectedValue,"Temparature Control","Set Temparature","Temp");
			//roomcomp[position].setProgress(speed);
		}		
		else if(selectedValue.getType().contains("Brightness")){
			showFanControl(selectedValue,"Brightness Control","Set Brightness","Ratio");
			//roomcomp[position].setProgress(speed);

		}
	}


	void showFanControl(final Device device, String Title,String msg, final String control ) {

		final AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle(Title);
		alert.setMessage(msg);

		LinearLayout linear=new LinearLayout(this);

		linear.setOrientation(1);
		final TextView text=new TextView(this);

		text.setText(control + ":");
		text.setPadding(10, 10, 10, 10);

		final SeekBar seek=new SeekBar(this);

		if(Title.contains("Temp")){
			seek.setMax(50);
		}
		seek.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			int progressChanged = 0;

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				text.setText(control + ":" + progressChanged + "%");
				//speed = progressChanged ;
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
				// TODO Auto-generated method stub
				progressChanged = progress ; 
				text.setText(control + ":" + progressChanged + "%");
				progress = progressChanged ;
				fanprogress = progress;
				//speed = progress;
			}

		});

		linear.addView(seek);
		linear.addView(text);

		alert.setView(linear);
		alert.setPositiveButton("OK",new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog,int id){
				//Toast.makeText(getApplicationContext(), "OK Pressed",Toast.LENGTH_LONG).show();

				/**** update the status of fan *****/
				//device.setOther(seek.getProgress());
				//updateview("");
				//finish();
			}
		});
		alert.setNegativeButton("Cancel",new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog,int id){
				//	 Toast.makeText(getApplicationContext(), "Cancel Pressed",Toast.LENGTH_LONG).show();
				//finish();
			}
		});

		alert.show();

	}


	public void updateview(final String str,final Boolean show)
	{

		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				adapter = new RoomControlAdapter(Main.db.getDeviceByRoom(roomid));
				setListAdapter(adapter);
				adapter.notifyDataSetChanged();
				if(show)Toast.makeText(Activity_Indoor_RoomControll.this, str , Toast.LENGTH_SHORT).show();
			}
		});
	}

	public class RoomControlAdapter extends ArrayAdapter<Device>{



		public RoomControlAdapter(ArrayList<Device> roomcomp) {

			super(Activity_Indoor_RoomControll.this, R.layout.activity_room_control_layout,roomcomp);
			//values = Main.db.getDeviceByRoom(roomid);
			//values = new ArrayList<Device>();
			values = roomcomp ;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			called++;
			Log.e("Here called","times:" + called);
			View row = convertView ;
			RoomDeviceHolder roomholder = null;

			if(row==null)
			{

				LayoutInflater inflater = (LayoutInflater) Activity_Indoor_RoomControll.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = inflater.inflate(R.layout.activity_room_control_layout, parent , false);
				roomholder = new RoomDeviceHolder();
				roomholder.holder_img = (ImageView)row.findViewById(R.id.room_control_logo);
				roomholder.holder_tx1 = (TextView)row.findViewById(R.id.room_control_label);
				roomholder.holder_on_off = (ImageView)row.findViewById(R.id.imageViewONOFF);
				row.setTag(roomholder);
			}
			else 
			{
				roomholder = (RoomDeviceHolder)row.getTag();
			}

			roomholder.holder_tx1.setText(values.get(position).getName());

			Log.e("RoomControllList:Name:Status",values.get(position).getName() + ":" + values.get(position).getOnf());
			Log.e("RoomControllList:Type:Status",values.get(position).getType() + ":"+ values.get(position).getOnf());

			Log.e("Position",""+position);

			Device device = values.get(position);

			if(device.getOnf() == 1){
				roomholder.holder_on_off.setImageResource(R.drawable.ic_on);
			}
			else roomholder.holder_on_off.setImageResource(R.drawable.ic_off);

			if(device.getType().contains("Light"))
			{

				//roomholder.holder_tx2.setText((device.geton().equals("1")?"ON":"OFF"));
				if(device.getOnf() == 1)
				{
					roomholder.holder_on_off.setImageResource(R.drawable.ic_on);
					roomholder.holder_img.setImageResource(R.drawable.ic_device_light_on);
				}
				else
				{
					roomholder.holder_on_off.setImageResource(R.drawable.ic_off);
					roomholder.holder_img.setImageResource(R.drawable.ic_device_light);
				}

			}
			else if(device.getType().contains("Fan")){
				//	roomholder.holder_img.setImageResource(R.drawable.color_fan);
				String p = String.valueOf(device.getOther());
				//roomholder.holder_tx2.setText("Speed:" + p + "%" );
			}
			else if(device.getType().contains("Temp")){
				//	roomholder.holder_img.setImageResource(R.drawable.color_temp);
				String p = String.valueOf(device.getOther());
				//roomholder.holder_tx2.setText(p + "~C" );
			}
			else if(device.getType().contains("Brightness")){
				//	roomholder.holder_img.setImageResource(R.drawable.color_brightness);
				String p = String.valueOf(device.getOther());
				//roomholder.holder_tx2.setText("Shadow:" +p + "%" );
			}
			else roomholder.holder_img.setImageResource(R.drawable.ic_device_light);

			return row;
		}

	}

	private void showLightConTrol(final Device device) {

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(Activity_Indoor_RoomControll.this);

		alertDialog.setTitle("Light Control");

		alertDialog.setMessage("what do you want?");

		/*
		 * Left Side Button
		 */
		alertDialog.setPositiveButton("ON", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int which) {


				//Toast.makeText(getApplicationContext(), "You clicked ON", Toast.LENGTH_SHORT).show();
				//room_function.set_light_packet(room_id, device.getId() ,"1", "100");
				//updateview();
				sendDataToRoom(roomid, device.getId(), 1, "100");

			}
		});
		/*
		 *  Right Side Button
		 */
		alertDialog.setNegativeButton("OFF", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				//room_function.set_light_packet(room_id, device.getId() ,"0", "000");
				sendDataToRoom(roomid, device.getId(), 0, "000");
			}
		});

		alertDialog.show();

	}
	public void sendDataToRoom(final int room_id,final int device_id,final int onf,final String other)
	{
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				List<com.mahfuz.room.Room> room_list = Main.db.getAllRooms();

				for(com.mahfuz.room.Room temp_room : room_list)
				{
					if(temp_room.getId() == room_id)
					{
						String room_ip = temp_room.getIp();

						String temp_rid = "0000" + room_id;
						temp_rid = temp_rid.substring(temp_rid.length()-4, temp_rid.length());

						String temp_did = "0000" + device_id;
						temp_did = temp_did.substring(temp_did.length()-4, temp_did.length());


						String pac = "A" + temp_rid + temp_did + onf + other;
						Log.d(TAG,pac);
						DataHandler.sendData(pac, room_ip, 2000);
					}
				}

			}
		}).start();

	}
	/*
	public class RoomArrayAdapter extends ArrayAdapter<Device> {

		//private Room[] values;

		public RoomArrayAdapter(List<Device> room) {
			super(Activity_Indoor_RoomControll.this,R.layout.activity_indoor_room_control_layout,room);
			values	 = room ;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			Log.e("In room list adapter","before adaper");

			LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.activity_room_list, parent , false);

			TextView tv   = (TextView)rowView.findViewById(R.id.tx_room_name);
		//	ImageView img = (ImageView)rowView.findViewById(R.id.logo);

			tv.setText(values.get(position).getName());

		//	img.setImageResource(R.drawable.ic_room);

			Log.e("In room list adapter","after adaper");

			return rowView;
		}

	}
	 */
	/*
	 *  to improve performance 
	 */
	static class RoomDeviceHolder
	{
		ImageView holder_img , holder_on_off; 
		TextView holder_tx1  , holder_tx2; 
	}
}