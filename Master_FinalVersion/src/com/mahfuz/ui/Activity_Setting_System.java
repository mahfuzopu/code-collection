package com.mahfuz.ui;

import com.example.main.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Activity_Setting_System extends Activity{
	Button bt_change_pass;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting_system);
		bt_change_pass = (Button)findViewById(R.id.bt_setting_system_change_pass);
		
		bt_change_pass.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent it = new Intent(Activity_Setting_System.this,Activity_Setting_System_ChangePass.class);
				startActivity(it);
				
			}
		});

	}
}
