package com.mahfuz.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.main.Main;
import com.example.main.R;
import com.mahfuz.outdoor.connection.JSONParser;
import com.mahfuz.outdoor.connection.URL;

public class Activity_outdoor_codeInput extends Activity{

	EditText etxt_code;
	Button bt_outdoor_code;
	AsyncTask<Void, Void, Boolean> asyncLogin;
	ProgressDialog progress ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//set content view
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_outdoor_codeinput);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title_bar_normal);
		
		
		etxt_code = (EditText)findViewById(R.id.edtxt_outdoor_activecode);
		bt_outdoor_code = (Button)findViewById(R.id.bt_outdoor_activate_code);


		bt_outdoor_code.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final String code = etxt_code.getText().toString().trim();
				WifiManager manager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
				WifiInfo info = manager.getConnectionInfo();
				final String address = info.getMacAddress().replaceAll(":", "");
				
				if(code.equals("") || address.equals(""))
				{
					//show a alert

					new AlertDialog.Builder(Activity_outdoor_codeInput.this).setIcon(R.drawable.fail)
					.setTitle("Error")
					.setMessage("No active internet connection found!")
					.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							finish();    
						}
					})
					.show();
				}
				else
				{
					//execute the serve code
					asyncLogin = new AsyncTask<Void, Void, Boolean>()
					{
						@Override
						protected void onPreExecute() 
						{
							// TODO Auto-generated method stub
							super.onPreExecute();
							progress = new ProgressDialog(Activity_outdoor_codeInput.this);
							progress.setTitle("Please wait");
							progress.setMessage("Sending...");
							progress.setCancelable(true);
							progress.show();


						}	
						@Override
						protected Boolean doInBackground(Void... arg0) 
						{
							// TODO Auto-generated method stub
							JSONParser parser = new JSONParser();
							//return parser.makeLoginRequest("http://dev.sinepulse.com/smarthome/service/dev/DataAPI.svc/data/login?user=companyadmin&pass=companyadmin");
							return parser.makeRequest(URL.getHomeActiveURL(address, code));
						}

						@Override
						protected void onPostExecute(Boolean result) 
						{
							// TODO Auto-generated method stub
							super.onPostExecute(result);
							if(result)
							{
								Toast.makeText(Activity_outdoor_codeInput.this, "Sent!!", Toast.LENGTH_SHORT).show();
							//	Intent it = new Intent(Activity_outdoor_codeInput.this,Activity_Outdoor_Initialization.class);
							//	startActivity(it);
								if(-1==Main.getsetInternetSetup(-1))finish();
								
							}
							else
							{
								//tx_error.setText("Error in communication!");
								Toast.makeText(Activity_outdoor_codeInput.this, "Error in communication!", Toast.LENGTH_SHORT).show();
							}
							progress.dismiss();
						}
					};
					asyncLogin.execute(null,null,null);
				}
			}
	});
}

}
