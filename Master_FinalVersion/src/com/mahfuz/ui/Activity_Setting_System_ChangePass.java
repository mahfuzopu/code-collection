package com.mahfuz.ui;

import com.example.main.AlertDialogManager;
import com.example.main.Main;
import com.example.main.R;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Activity_Setting_System_ChangePass extends Activity{

	Button bt_okay,bt_cancel ;
	EditText old_pass, new_pass1, new_pass2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_setting_system_change_pass);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title_bar);
		
		////////////////////////////////////////////////////////////////
		
		bt_okay = (Button)findViewById(R.id.bt_setting_system_change_pass_okay);
		bt_cancel = (Button)findViewById(R.id.bt_system_setting_change_pass_cancel);
		old_pass = (EditText)findViewById(R.id.etx_old_pass);
		new_pass1 = (EditText)findViewById(R.id.etx_new_pass1);
		new_pass2 = (EditText)findViewById(R.id.etx_new_pass2);
		
		
		old_pass.setHint("Enter current password");
		new_pass1.setHint("New password");
		new_pass2.setHint("Retype new password");
		
		
		
		bt_okay.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String old_p = old_pass.getText().toString().trim();
				String new_p1 = new_pass1.getText().toString().trim();
				String new_p2 = new_pass2.getText().toString().trim();
				
				if(old_p.length()>0 && new_p1.length()>0 && new_p2.length()>0)
				{
					//process 
					
					
					SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(Main.main);
					String pass = pref.getString("PASS", "");
					
					
					if(pass.equals(""))
					{
						
					}
					else
					{
						if(new_p1.equals(new_p2))
						{
							SharedPreferences.Editor editor = pref.edit();
							editor.remove("PASS");
							editor.commit();
							
							editor.putString("PASS", new_p2);
							editor.commit();
							
							Toast.makeText(Activity_Setting_System_ChangePass.this, "Done.", Toast.LENGTH_SHORT).show();
							
							finish();
							
						}
						else
						{
							AlertDialogManager alert = new AlertDialogManager();
							alert.showAlertDialog(Activity_Setting_System_ChangePass.this, "Error!", "Password does not match.", false);			
						}
					}
				}
				else
				{
					AlertDialogManager alert = new AlertDialogManager();
					alert.showAlertDialog(Activity_Setting_System_ChangePass.this, "Information missing", "empty field.", false);
					
				}
			}
		});
		

		bt_cancel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		
	}
	
}
