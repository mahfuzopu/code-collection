package com.mahfuz.ui.updateholder;

import com.mahfuz.ui.Activity_Indoor_RoomControll;
import com.mahfuz.ui.Activity_Indoor_RoomList;

public class uiUpdate {
	public Activity_Indoor_RoomList roomlist 		= null;
	public Activity_Indoor_RoomControll devicelist 	= null;
	public uiUpdate(){}
	
	public void setRoomListHolder(Activity_Indoor_RoomList r)
	{
		this.roomlist = r ;
	}
	public void setDeviceListHolder(Activity_Indoor_RoomControll r)
	{
		this.devicelist = r ;
	}
	public void update_room_list(String str,Boolean is)
	{
		if(roomlist!=null)roomlist.updateview(str, is);
	}
	public void update_device_list(String str,Boolean is)
	{
		if(devicelist!=null)devicelist.updateview(str, is);
	}
}
