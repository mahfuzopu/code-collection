package com.mahfuz.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.main.Data;
import com.example.main.Main;
import com.example.main.R;
import com.mahfuz.outdoor.connection.JSONParser;
import com.mahfuz.outdoor.connection.URL;

public class Activity_Outdoor_Initialization extends Activity{

	TextView txt_message ;
	EditText edtxt_email;
	Button bt_initialization;
	AsyncTask<Void, Void, Boolean> mInitialization ;
	ProgressDialog progress ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_outdoor_initialization);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title_bar_normal);
		
		

		edtxt_email = (EditText)findViewById(R.id.etxt_outdoor_email);
		
		txt_message = (TextView)findViewById(R.id.txt_outdoor_initialization);
		
		bt_initialization = (Button)findViewById(R.id.bt_outdoor_initialization);
		
		bt_initialization.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				final String gcmregid = Main.GetSetGcmIRegId("00000");
				
				WifiManager manager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
				WifiInfo info = manager.getConnectionInfo();
				String tadd = info.getMacAddress();
				
				final String address = tadd.replaceAll(":", "");
				
				Log.e("MAC ADDRESS", address);
				
				final String email = edtxt_email.getText().toString().trim();
				
				if(address.equals("") || gcmregid.equals("00000") || email.equals(""))
				{
					txt_message.setText("Error in fetching MAC or GCM Registration ID");
				}
				else
				{
					//txt_message.setText("Your request is sent to server!");
					txt_message.setText("Your request is sent to server! where mac id=" + address + " GCM reg id=" + gcmregid);
					/*
					 * make a asynchronus reques to send the data to server
					 */
					mInitialization = new AsyncTask<Void, Void, Boolean>()
					{
						@Override
						protected void onPreExecute() {
							// TODO Auto-generated method stub
							super.onPreExecute();
							progress = new ProgressDialog(Activity_Outdoor_Initialization.this);
							progress.setTitle("Please wait.");
							progress.setMessage("Sending request...");
							progress.show();
							
						}

						@Override
						protected Boolean doInBackground(Void... params) {
							// TODO Auto-generated method stub
							//hit the url
							JSONParser parser = new JSONParser();
							//return false ;
							return parser.makeRequest(URL.getInitializationURL(address,gcmregid,email));
						}
						@Override
						protected void onPostExecute(Boolean result) {
							// TODO Auto-generated method stub
							super.onPostExecute(result);
							progress.cancel();
							
							if(result)
							{
								Toast.makeText(Activity_Outdoor_Initialization.this, "sent!", Toast.LENGTH_SHORT).show();
								Intent it = new Intent(Activity_Outdoor_Initialization.this,Activity_outdoor_codeInput.class);
								startActivity(it);
								finish();
							}
							else
							{
								Toast.makeText(Activity_Outdoor_Initialization.this, "error in sendind!", Toast.LENGTH_SHORT).show();
							}
						}
						
					};
							
					mInitialization.execute(null,null,null);
					
				}
			}
		});		
	}
}
