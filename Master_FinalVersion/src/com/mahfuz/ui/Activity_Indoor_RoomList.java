package com.mahfuz.ui;


import java.util.List;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.main.Main;
import com.example.main.R;
import com.mahfuz.room.Room;
import com.mahfuz.ui.updateholder.uiUpdate;

public class Activity_Indoor_RoomList extends ListActivity {
	
	RoomArrayAdapter adapter ; 
	public List<Room> values ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//setTitle("Available Room List");
		
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		
		Main.uiupdate = new uiUpdate();
		Main.uiupdate.setRoomListHolder(Activity_Indoor_RoomList.this);
		
		//Main.ui_update.setRoom_list(Activity_Indoor_RoomList.this);
	
		adapter = new RoomArrayAdapter(Main.db.getAllRooms());
		setListAdapter(adapter);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title_bar);
		
		
	}
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		
		//String selectedValue = (String) getListAdapter().getItem(position);
		//Toast.makeText(this, selectedValue, Toast.LENGTH_SHORT).show();
		
		
		Intent it = new Intent(Activity_Indoor_RoomList.this,Activity_Indoor_RoomControll.class);
		it.putExtra("roomid",values.get(position).getId());	// here position is the room number // will be change according to room propertices
		//it.putExtra("position",String.valueOf(position));
		startActivity(it);
		
	}
	public void updateview(final String str,final Boolean show){
		
		this.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
	//			adapter = new RoomArrayAdapter(Activity_Indoor_RoomList.this,MainActivity.controller.getRoom_list());
	//			setListAdapter(adapter);
	//			adapter.notifyDataSetChanged();
	//			if(show)Toast.makeText(Activity_Indoor_RoomList.this, str , Toast.LENGTH_LONG).show();
			}
		});
	}
	public class RoomArrayAdapter extends ArrayAdapter<Room> {
		
		private Context  context ; 
		//private Room[] values;
		
		
		
		public RoomArrayAdapter(List<Room> room) {
			super(Activity_Indoor_RoomList.this,R.layout.activity_room_list,room);
			this.context = context ; 
			//this.values = new ArrayList<Room>();
			values	 = room ;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			Log.e("In room list adapter","before adaper");
			
			LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.activity_room_list, parent , false);
			
			TextView tv   = (TextView)rowView.findViewById(R.id.tx_room_name);
			TextView tv1   = (TextView)rowView.findViewById(R.id.tx_room_active);
		//	ImageView img = (ImageView)rowView.findViewById(R.id.logo);
			
			tv.setText(values.get(position).getName());
			
		//	img.setImageResource(R.drawable.ic_room);
			
			Log.e("In room list adapter","after adaper");
			
			tv1.setText((values.get(position).getActive()==1)?"Active":"Inactive");
			
			return rowView;
		}

	}	
}