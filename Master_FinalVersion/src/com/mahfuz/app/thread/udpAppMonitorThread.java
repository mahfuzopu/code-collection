package com.mahfuz.app.thread;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import android.util.Log;

import com.example.main.Utils;
import com.mahfuz.connection.DataHandler;

public class udpAppMonitorThread implements Runnable {

	static public Boolean udpAppThreadRunning ;
	
	public udpAppMonitorThread(){
		
		
		udpAppThreadRunning = true ;
		new Thread(this).start();
		//MainActivity.main.show_tx1("After udp App Thread!");
		
	}
	
	@Override
	public void run() {
		
		DatagramSocket ds = null ; 			
		//int server_port = 44444;
		byte[] message = new byte[200];
		DatagramPacket p = new DatagramPacket(message, message.length);
		try {
			ds = new DatagramSocket(12345);
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			udpAppThreadRunning = false ;
		}
		
		while(udpAppThreadRunning){
			
			try{
				try{
					
	//				MainActivity.main.show_tx3("waiting for a app...");
					
					Log.e("in App ip init","in app ip init");
					ds.receive(p);// set timeOut to 2000ms
					
					
					final int 			 port 	= 33333 ;
					final InetAddress	 ip	= p.getAddress();
					
		//			MainActivity.main.show_tx2("a packet receieved!");
					
					
					DataHandler.sendData(Utils.getIPAddress(true).toString().trim(),ip.getHostAddress(), port);
					
					
					/******************** checking of data received ***********************/
					
		//			String sender_ip = ip.getHostAddress();
					
				//	MainActivity.main.show_tx2("A msg from app--ip:" + sender_ip);

					
				}catch(SocketTimeoutException e)
				{
				//	MainActivity.main.show_tx1("Time Out exception");
					//if(!ds.isClosed())ds.close();
				}
				
				//if(!ds.isClosed())ds.close();
				
			}catch(Exception e){
				udpAppThreadRunning = false ;
				e.printStackTrace();
				if(!ds.isClosed())ds.close();
			}
		}
		
	}
}
