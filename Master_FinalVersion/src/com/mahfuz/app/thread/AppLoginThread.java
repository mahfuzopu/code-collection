package com.mahfuz.app.thread;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;

import com.example.main.Data;
import com.example.main.Main;
import com.example.users.User;

public class AppLoginThread implements Runnable {

	static Boolean AppLoginThreadisRunning = false ;
	ServerSocket sc = null;
	public AppLoginThread() {
		// TODO Auto-generated constructor stub
		/****************************************/
		AppLoginThreadisRunning = true ;
		new Thread(this).start();
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
	 try
	 {
		if(sc!=null)
		{
			if(!sc.isClosed())sc.close();
		}
		sc = new ServerSocket(Data.port_app_login);
		
		while(AppLoginThreadisRunning)
		{
			Socket cs = sc.accept();
			BufferedReader in = new BufferedReader(new InputStreamReader(cs.getInputStream()));
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(cs.getOutputStream()));
			
			String datain = in.readLine();
			
			//verify the login request
			
			StringTokenizer token = new StringTokenizer(datain,":");
			if(token.countTokens()==2){
				String _name = token.nextToken();
				String _pass = token.nextToken();
				
				//now verify
				
				User u = Main.db.VerifyUserLogin(_name, _pass);
				
				if(u!=null){
					out.write(u.getUserId());
				}
				else out.write("0");

				out.flush();
				
			}
			out.close();
			in.close();
			
			if(!cs.isClosed())cs.close();
		}
	 }catch(Exception e)
	 {
		 
	 }
	}

}
