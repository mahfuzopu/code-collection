package com.mahfuz.database;

import java.util.ArrayList;
import java.util.List;

import com.example.users.User;
import com.mahfuz.room.Device;
import com.mahfuz.room.Room;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper{

	public String TAG = "DatabaseHelper";
	private final static int  DB_VERSION 			= 2 ; 
	private final static String DB_NAME 			= "HOMEAUTOMATION";

	//TABLES
	private final static String TABLE_ROOM 			= "ROOM";
	private final static String TABLE_USER 			= "USER";
	private final static String TABLE_DEVICE 		= "DEVICE";
	private final static String TABLE_ROOM_USER 	= "ROOM_USER";
	private final static String TABLE_ROOM_DEVICE 	= "ROOM_DEVICE";

	// KEYS
	private final static String KEY_ID = "id";
	private final static String KEY_NAME = "name";
	private final static String KEY_IP = "ip";
	private final static String KEY_ACTIVE = "active";
	private final static String KEY_TYPE = "type";
	private final static String KEY_OTHER = "other";
	private final static String KEY_ONF = "onf";
	private final static String KEY_PASS = "password";

	private final static String KEY_ROOM_ID = "room_id";
	private final static String KEY_DEVICE_ID = "device_id";
	private final static String KEY_USER_ID = "user_id";



	//create table SQL
	private final static String CREATE_TABLE_ROOM 	 = "CREATE TABLE " + TABLE_ROOM + " ( " + KEY_ID + "  INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_NAME + " TEXT , " 
			+ KEY_IP + " TEXT , " + KEY_ACTIVE + " INTEGER " +" )";


	private final static String CREATE_TABLE_USER	 = "CREATE TABLE " + TABLE_USER + " ( " + KEY_ID + "  INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_NAME + " TEXT , " 
			+ KEY_PASS + " TEXT "+" )";

	private final static String CREATE_TABLE_DEVICE  = "CREATE TABLE " + TABLE_DEVICE + " ( " + KEY_ID + "  INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_DEVICE_ID + " INTEGER, "+ KEY_ROOM_ID + " INTEGER , "+KEY_NAME + " TEXT , " 
			+ KEY_TYPE + " TEXT , " + KEY_OTHER +" TEXT , " + KEY_ONF + " INTEGER " +" )";


	private final static String CREATE_TABLE_ROOM_USER  = "CREATE TABLE " + TABLE_ROOM_USER + " ( " + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_ROOM_ID + " INTEGER , " 
			+ KEY_USER_ID + " INTEGER " +" )";


	private final static String CREATE_TABLE_ROOM_DEVICE  = "CREATE TABLE " + TABLE_ROOM_DEVICE + " ( " + KEY_ID + "  INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_ROOM_ID + " INTEGER , " 
			+ KEY_DEVICE_ID + " INTEGER " +" )";


	public DatabaseHelper(Context context) {
		super(context, DB_NAME, null , DB_VERSION);
		Log.d(TAG, "in constructor");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub

		//create table
		db.execSQL(CREATE_TABLE_ROOM);
		db.execSQL(CREATE_TABLE_USER);
		db.execSQL(CREATE_TABLE_DEVICE);

		db.execSQL(CREATE_TABLE_ROOM_DEVICE);
		db.execSQL(CREATE_TABLE_ROOM_USER);
		
		Log.d(TAG, "in OnCreate");

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROOM);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DEVICE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROOM_USER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROOM_DEVICE);

		this.onCreate(db); 

	}

	/// database functions

	public long createDevice(Device device)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_NAME, device.getName());
		values.put(KEY_TYPE, device.getType());
		values.put(KEY_OTHER, device.getOther());
		values.put(KEY_ONF, device.getOnf());
		values.put(KEY_DEVICE_ID, device.getId());
		values.put(KEY_ROOM_ID, device.getRoomid());

		long id = db.insert(TABLE_DEVICE, null, values);

		//device.setId((int)id);

		return id;
	}
	public long createRoom(Room r)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_NAME, r.getName());
		values.put(KEY_IP, r.getIp());
		values.put(KEY_ACTIVE, r.getActive());

		long id = db.insert(TABLE_ROOM, null, values);

		r.setId((int)id);

		return id;
	}
	public long createUser(User u)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_NAME, u.getName());
		values.put(KEY_PASS, u.getPassword());

		long id = db.insert(TABLE_USER, null, values);

		u.setUserId((int)id);

		Log.e(TAG, "A user created with id" + id);
		
		return id ;
	}
	/************************************ GET ALL ROOM ************************************************/
	public List<Room> getAllRooms()
	{
		List<Room> rooms = new ArrayList<Room>();
		String selectQuery = "SELECT  * FROM " + TABLE_ROOM;
		Log.e(TAG,selectQuery);
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		if (c.moveToFirst()) {
			do {
				Room td = new Room();
				td.setId(c.getInt((c.getColumnIndex(KEY_ID))));
				td.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				td.setIp(c.getString(c.getColumnIndex(KEY_IP)));
				td.setActive(c.getInt(c.getColumnIndex(KEY_ACTIVE)));

				rooms.add(td);

			} while (c.moveToNext());
		}
		return rooms;
	}
	/*
	 *  SET room Inactive
	 */
	public void setRoomInactive(int room_id)
	{
	    SQLiteDatabase db = this.getWritableDatabase();
	    
	    ContentValues values = new ContentValues();
	    values.put(KEY_ACTIVE, 0);
	 
	    // updating row
	    long id = db.update(TABLE_ROOM, values, KEY_ID+  " = ? ",new String[] { String.valueOf(room_id) });
	    
	    Log.e(TAG, "set all room deactive : row updated=" + id);	
	}
	/************************************ GET ALL DEVICE LIST ************************************************/
	public List<Device> getAllDevice(int room_id)
	{
		List<Device> devices = new ArrayList<Device>();
		String selectQuery = "SELECT * FROM " + TABLE_DEVICE + " WHERE " + KEY_ROOM_ID + "='" + room_id +"'";
		Log.e(TAG,selectQuery);
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		if (c.moveToFirst()) {
			do {
				Device td = new Device();
				td.setId(c.getInt((c.getColumnIndex(KEY_DEVICE_ID))));
				td.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				td.setType((c.getString(c.getColumnIndex(KEY_TYPE))));
				td.setOther((c.getString(c.getColumnIndex(KEY_OTHER))));
				td.setOnf(c.getInt(c.getColumnIndex(KEY_ONF)));
				td.setRoomid(c.getInt(c.getColumnIndex(KEY_ROOM_ID)));

				devices.add(td);				

			} while (c.moveToNext());
		}
		return devices;
	}
	/*
	 *  UPDATE device information 
	 */
	public boolean UpdateDeveiceInfor(Device d)
	{
		SQLiteDatabase db = this.getWritableDatabase();
	    ContentValues values = new ContentValues();
	    values.put(KEY_ONF, d.getOnf());
	    values.put(KEY_OTHER, d.getOther());
	    
	    // updating row
	    long id = db.update(TABLE_DEVICE, values, KEY_ROOM_ID + " = ? AND "+  KEY_DEVICE_ID + " = ? ",new String[] { String.valueOf(d.getRoomid()), String.valueOf(d.getId()) });
	    Log.e(TAG, "change device status;row updated=" + id + "room id=" + d.getRoomid() + " , Device id=" + d.getId());
		
	    return id>0;
	}
	/*
	 *  DETETE Device of room
	 */
	public void DeleteAllDeviceinRoom(int room_id)
	{
		SQLiteDatabase db = this.getWritableDatabase();
	    db.delete(TABLE_DEVICE, KEY_ROOM_ID + " = ?",
	            new String[] { String.valueOf(room_id) });
	    Log.e(TAG, "all the device in room " + room_id + " is deleted");
	}
	/*
	 *  DETETE  room 
	 */
	public void DeleteRoom(int room_id)
	{
		SQLiteDatabase db = this.getWritableDatabase();
	    db.delete(TABLE_ROOM, KEY_ID + " = ?",
	            new String[] { String.valueOf(room_id) });
	    Log.e(TAG, "A room is deleted with id = " + room_id);
	}
	/************************************ GET ALL USER ************************************************/
	public List<User> getAllUser()
	{
		List<User> users = new ArrayList<User>();
		String selectQuery = "SELECT * FROM " + TABLE_USER ;
		Log.e(TAG,selectQuery);
		SQLiteDatabase db = this.getReadableDatabase();
		
		Cursor c = db.rawQuery(selectQuery, null);

		if (c.moveToFirst()) {
			do {
				User td = new User();
				td.setUserId(c.getInt((c.getColumnIndex(KEY_ID))));
				td.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				td.setPassword(((c.getString(c.getColumnIndex(KEY_PASS)))));

				users.add(td);
				
				Log.d(TAG,"User Name" + td.getName() +  " & ID="+ td.getUserId());

			} while (c.moveToNext());
		}
		return users;
	}
	/*
	 *  GET User Login Auth
	 */
	public User VerifyUserLogin(String name,String pass)
	{
		User u = null ;
		String selectQuery = "SELECT  * FROM " + TABLE_USER +" WHERE " + KEY_NAME +" = '" + name + "' AND "+
							  KEY_PASS + " = '" + pass + "'";
		
		Log.e(TAG,selectQuery);
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);
		
		if (c.moveToFirst()) {
			do {
				u = new User();
				String _name = c.getString(c.getColumnIndex(KEY_NAME));
				String _pass = c.getString(c.getColumnIndex(KEY_PASS));
				int _id   =    c.getInt(c.getColumnIndex(KEY_ID));
				u.setName(_name);
				u.setPassword(_pass);
				u.setUserId(_id);
				
			} while (c.moveToNext());
		}		
		return u ;
	}
	/************************************ DELETE A USER  ************************************************/
	public void DeleteUser(int user_id)
	{
		SQLiteDatabase db = this.getWritableDatabase();
	    db.delete(TABLE_USER, KEY_ID + " = ?",
	            new String[] { String.valueOf(user_id) });
	    Log.e(TAG, "A user is deleted with id!" + user_id);
	}
	
	/************************************ GET DEVICE BY ROOM ************************************************/
	public ArrayList<com.mahfuz.room.Device> getDeviceByRoom(int room_id)
	{
		ArrayList<com.mahfuz.room.Device> devices = new ArrayList<Device>();
	//	String selectQuery = "SELECT  * FROM " + TABLE_DEVICE +" d, " + TABLE_ROOM + " r, " + TABLE_ROOM_DEVICE + " rd WHERE r." + KEY_ID +" = '" + room_id + "' AND "+
	//						 " r." + KEY_ID + " =  rd." + KEY_ROOM_ID + " AND d."+ KEY_ID + " = " + "rd."+ KEY_DEVICE_ID;
		
		String selectQuery = "SELECT  * FROM " + TABLE_DEVICE + " WHERE " + KEY_ROOM_ID + "='" +room_id+ "'" ;
				
		
		Log.e(TAG,selectQuery);
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		if (c.moveToFirst()) {
			do {
				Device td = new Device();
				td.setId(c.getInt((c.getColumnIndex(KEY_DEVICE_ID))));
				td.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				td.setType((c.getString(c.getColumnIndex(KEY_TYPE))));
				td.setOther((c.getString(c.getColumnIndex(KEY_OTHER))));
				td.setOnf(c.getInt(c.getColumnIndex(KEY_ONF)));

				devices.add(td);
				Log.e(TAG,"Device id :" + td.getId());

			} while (c.moveToNext());
		}
		
		return devices;
	}
	/************************************ GET ROOM LIST BY USER ************************************************/
	
	public List<Room> getRoomListbyUser(int user_id)
	{
		List<Room> rooms = new ArrayList<Room>();
		String selectQuery = "SELECT  * FROM " + TABLE_USER +" u, " + TABLE_ROOM + " r, " + TABLE_ROOM_USER + " ru, WHERE u." + KEY_ID +" = '" + user_id + "' AND "+
				 " u." + KEY_ID + " =  ru." + KEY_USER_ID + " AND r."+ KEY_ID + " = " + "ru."+ KEY_ROOM_ID;

		Log.e(TAG,selectQuery);
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		if (c.moveToFirst()) {
			do {
				Room td = new Room();
				td.setId(c.getInt((c.getColumnIndex(KEY_ID))));
				td.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				td.setIp(c.getString(c.getColumnIndex(KEY_IP)));
				td.setActive(c.getInt(c.getColumnIndex(KEY_ACTIVE)));

				rooms.add(td);

			} while (c.moveToNext());
		}
		return rooms;		
	}
	public void setAllRoomDeactive()
	{
	    SQLiteDatabase db = this.getWritableDatabase();
	    
	    ContentValues values = new ContentValues();
	    values.put(KEY_ACTIVE, 0);
	 
	    // updating row
	    long id = db.update(TABLE_ROOM, values, null,null);
	    Log.e(TAG, "set all room deactive : row updated=" + id);
	    
	}
	/*************************************** SET ROOM DEACTIVE ***********************************/
	public void setRoomDeActive(int room_id)
	{
	    SQLiteDatabase db = this.getWritableDatabase();
	    
	    ContentValues values = new ContentValues();
	    values.put(KEY_ACTIVE, 0);
	 
	    // updating row
	    long id = db.update(TABLE_ROOM, values, KEY_ID + " = ?", new String[] { String.valueOf(room_id) });
	    Log.e(TAG, "set room deactive : row updated=" + id);
	    
	}
	/*************************************** SET ROOM ACTIVE **************************************/
	public void setRoomActive(int room_id)
	{
	    SQLiteDatabase db = this.getWritableDatabase();
	    
	    ContentValues values = new ContentValues();
	    values.put(KEY_ACTIVE, 1);
	 
	    // updating row
	    long id = db.update(TABLE_ROOM, values, KEY_ID + " = ?", new String[] { String.valueOf(room_id) });
	    Log.e(TAG, "set Room active : row updated=" + id);
	}
	
	/************************************ CLOSE DB ************************************************/
	public void closeDB()
	{
		SQLiteDatabase db = this.getReadableDatabase();
		if(db!=null && db.isOpen())
		{
			db.close();
		}
	}
/*
 * assign user to room
 * @param user
 * @param room
 */
	public boolean assignUserToRoom(User u, Room room) {
		// TODO Auto-generated method stub
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_USER_ID, u.getUserId());
		values.put(KEY_ROOM_ID, room.getId());

		long id = db.insert(TABLE_ROOM_USER, null, values);

		return id>0;
	}

}
