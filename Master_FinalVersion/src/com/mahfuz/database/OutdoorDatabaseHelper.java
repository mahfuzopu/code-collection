package com.mahfuz.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class OutdoorDatabaseHelper extends SQLiteOpenHelper{

	public String TAG = "OutdoorDatabaseHelper";
	private final static int VERSION = 1;
	private final static String DB_NAME = "OUTDOOR_HOMEAUTOMATION";
	
	// table names
	private final static String 	TABLE_TYPE 				= "TYPE";
	private final static String 	TABLE_PROPERTY 			= "PROPERTY";
	private final static String 	TABLE_TYPE_PROPERTY 	= "TYPE_PROPERTY";
	
	//keys
	private final static String 	KEY_ID 					= "id";
	private final static String 	KEY_NAME 				= "name";
	private final static String 	KEY_ACTIVE 				= "active";
	private final static String 	KEY_TYPE_ID 			= "type_id";
	private final static String 	KEY_PROPERTY_ID 		= "property_id";
	
	
	private final static String CREATE_TABLE_TYPE 	 	 	= "CREATE TABLE " + TABLE_TYPE + " ( " + KEY_ID + "  INTEGER PRIMARY KEY , " + KEY_NAME + " TEXT , " + KEY_ACTIVE + " INTEGER " +" )";

	private final static String CREATE_TABLE_PROPERTY 	 	= "CREATE TABLE " + TABLE_PROPERTY  + " ( " + KEY_ID + "  INTEGER PRIMARY KEY , " + KEY_NAME + " TEXT )";
	
	private final static String CREATE_TABLE_TYPE_PROPERTY 	= "CREATE TABLE " + TABLE_TYPE_PROPERTY + " ( " + KEY_TYPE_ID + "  INTEGER, "  + KEY_PROPERTY_ID + " INTEGER " +" )";
	
	
	
	
	public OutdoorDatabaseHelper(Context context) {
		super(context, DB_NAME, null, VERSION);
	
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		//create table
		db.execSQL(CREATE_TABLE_TYPE);
		db.execSQL(CREATE_TABLE_PROPERTY);
		db.execSQL(CREATE_TABLE_TYPE_PROPERTY);

		
		Log.d(TAG, "in OnCreate outdoor database");
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TYPE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROPERTY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TYPE_PROPERTY);

		this.onCreate(db); 
	}
	//database functions
	public long insertType(int id,String name,int active)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_ID, id);
		values.put(KEY_NAME, name);
		values.put(KEY_ACTIVE, active);
		long tid = db.insert(TABLE_TYPE, null, values);
		
		if(tid>=0)Log.d("GCM","Type Inserted!");
		else Log.d("GCM","Type Insertion error!");
		
		return tid;
	}
	public long insertProperty(int id,String name)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_ID, id);
		values.put(KEY_NAME, name);
		long tid = db.insert(TABLE_PROPERTY, null, values);
		
		if(tid>=0)Log.d("GCM","Property Inserted!");
		else Log.d("GCM","Property Insertion error!");
		
		return tid;
	}
	public long insertTypeProperty(int tid,int pid)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_TYPE_ID, tid);
		values.put(KEY_PROPERTY_ID, pid);
		long id = db.insert(TABLE_TYPE_PROPERTY, null, values);
		
		if(id>=0)Log.d("GCM","TP Inserted!");
		else Log.d("GCM","TP Insertion error!");
		
		return id;
	}
}
