package com.mahfuz.prev.app;

import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

import android.util.Log;

import com.example.main.Data;
import com.example.main.Main;
import com.example.room.Device;
import com.example.room.Room;
import com.example.room.RoomController;

public class MasterToAppInitThread implements Runnable {
	
	private static final String TAG = "MasterToAppInitThread";
	private ServerSocket server = null ;
	ObjectOutputStream out		= null ;
	
	public static Boolean masterToAppInitThreadRunning = false ;
	
	public MasterToAppInitThread(){
		
		/***********************************************/
			masterToAppInitThreadRunning = true ;
			new Thread(this).start();
		/***********************************************/
		
	}
	@Override
	public void run() {
		try{
			if(server==null)server = new ServerSocket(1111);
			else {
				if(!server.isClosed())server.close();
				server = new ServerSocket(Data.port_MasterToAppInitThread);
			}
		}catch(Exception e){}
		
		while(masterToAppInitThreadRunning){
			try{
				
				Socket cs = server.accept();
				out = new ObjectOutputStream(cs.getOutputStream());
				
				String ip = cs.getInetAddress().getHostAddress();
				
				/*
				 * 1. create a room controller class
				 * 2. assign room and device to controller
				 * 3. write the controller to application
				 */
				
				RoomController con = new RoomController();
				
				List<com.mahfuz.room.Room> room = Main.db.getAllRooms();
				
				for(com.mahfuz.room.Room tr:room)
				{
					com.example.room.Room prev_room = new com.example.room.Room();
					prev_room.setId(String.valueOf(tr.getId()));
					prev_room.setName(tr.getName());
					
					Log.e(TAG,"Room id : == " + prev_room.getId());
					
					List<com.mahfuz.room.Device> dev = Main.db.getAllDevice(tr.getId());
					
					for(com.mahfuz.room.Device td: dev)
					{
						com.example.room.Device prev_dev = new com.example.room.Device();
						prev_dev.setId(String.valueOf(td.getId()));
						prev_dev.setName(td.getName());
						prev_dev.setType("Light");
						prev_dev.setOn(String.valueOf(td.getOnf()));
						
						
						//add the device to room
						Log.e(TAG,"Device id : == " + prev_dev.getId());
						
						prev_room.add_device(prev_dev);
					}
					
					con.add_room(prev_room);
				}
				
				
				//dummy device/room/room_controoler
				/*
				Device d1 = new Device();
				d1.setId("0001"); d1.setName("Device1");d1.setOn("1");d1.setType("Light");
				
				Device d2 = new Device();
				d2.setId("0002"); d2.setName("Device2");d2.setOn("0");d2.setType("Light");
				
				Device d3 = new Device();
				d3.setId("0003"); d3.setName("Device3");d3.setOn("1");d3.setType("Light");
				
				
				Room r = new Room();
				r.add_device(d1);
				r.add_device(d2);
				r.add_device(d3);
				
				r.setId("0001");
				r.setName("my Room!");
				
				RoomController con = new RoomController();
				con.add_room(r);
				
				*/
				out.writeObject(con);
				out.flush();
				if(!cs.isClosed())cs.close();
				
				Log.d(TAG,"a room controoler written!");
				
				
				//this part will not be same
				
				if(!Main.active_app_list.contains(ip))Main.active_app_list.add(ip);
				
				Log.d(TAG,"A app is added to list!");
				
				
				/*
				out.writeObject(MainActivity.main.controller);
				out.flush();
				
				//MainActivity.main.show_tx3("A new Room COntroller is written!");
				
				if(!cs.isClosed())cs.close();
				
				if(!MainActivity.app_ip_lis.contains(ip)){
					MainActivity.app_ip_lis.add(ip);
					MainActivity.main.show_tx1("A new app ip:" + ip);
				}
				*/
				
			}catch(Exception e){
				masterToAppInitThreadRunning = false ;
			}
		}
	}
}