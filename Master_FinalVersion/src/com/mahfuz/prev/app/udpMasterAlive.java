package com.mahfuz.prev.app;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.StringTokenizer;

import android.util.Log;

import com.example.main.Utils;

public class udpMasterAlive implements Runnable {
	
	final static String TAG = "udpMasterAlive";
	
	public static boolean isUdpMasterAlive = false ; 
	public udpMasterAlive() {
		
		isUdpMasterAlive = true ;
		new Thread(this).start();
	}
	
	
	@Override
	public void run() {
		//udp receiever
		DatagramSocket ds = null  ;
		int udp_server_port = 54321 ;
		byte[] message = new byte[200];
		
		message[0] = (byte)'I';
		
		try {
			
			ds  = new DatagramSocket(udp_server_port);
		
		} catch (SocketException e) {	e.printStackTrace();	} 			

		
		while(isUdpMasterAlive){
			
			try {
				
                //String ip = Inet4Address.getLocalHost().getHostAddress();
                
                String ip = Utils.getIPAddress(true);
                
                Log.e("udp master alive thread!", ip);
                
                StringTokenizer strtok = new StringTokenizer(ip,".");
                
                String s1 = strtok.nextToken();
                String s2 = strtok.nextToken();
                String s3 = strtok.nextToken();
                
                String new_ip = s1 + "." + s2 + "." + s3 + ".255";
                
                InetAddress local = InetAddress.getByName(new_ip);
                
                Log.e("In udp Serverip Init",new_ip);
                
				ds.send(new DatagramPacket(message, message.length , local ,udp_server_port) );
				Thread.sleep(1000);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				break;
			}
			Log.e("Tag", "Hello here_12");
				
		}
	}

}
