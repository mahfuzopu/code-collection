package com.mahfuz.prev.app;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import android.util.Log;

import com.example.dataformat.DataFormat;
import com.example.main.Data;
import com.example.main.Main;
import com.example.users.User;

public class AppToMasterUserLoginThread implements Runnable {

	Boolean isinUserLoginAuthRunning = false ;
	ServerSocket sc = null ;
	Socket cs = null;
	final static String TAG = "AppToMasterUserLoginThread";


	public AppToMasterUserLoginThread() {
		isinUserLoginAuthRunning = true ;
		new Thread(this).start();
	}
	@Override
	public void run() {
		try
		{
			if(sc!=null){
				if(sc.isClosed())sc.close();
			}
			sc = new ServerSocket(Data.port_AppToMasterUserLogin);

		}catch(Exception e)
		{
			isinUserLoginAuthRunning = false ;
			Log.d("isinUserLoginAuthRunning",e.toString());
		}
		while(isinUserLoginAuthRunning)
		{
			try
			{
				Log.d("inUserLoginAuth","waiting for user login....");
				cs = sc.accept();


				BufferedReader br = new BufferedReader(new InputStreamReader(cs.getInputStream()));
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(cs.getOutputStream()));


				String datain = br.readLine();


				List<String> l = new ArrayList<String>();
				//l.clear();

				Log.d("inUserLoginAuth",datain);

				StringTokenizer strtok = new StringTokenizer(datain, ",");

				while (strtok.hasMoreTokens()) {
					String object = strtok.nextToken();
					l.add(object);
					Log.d("inUserLoginAuth",object);
				}
				String user_name = "null" , user_ps = "null" , user_id = "null"; 

				Boolean sucess = false ;

				if(l.get(3).equals("CMD") && l.get(4).equals("001"))
				{
					user_name = l.get(5);
					user_ps = l.get(6);
					Log.d("inUserLogin", user_name + ":" + user_ps ) ;
					
					/*
					 * 1. fetch data from database
					 * 2. construct packet to send
					 * 3. send user id also if verified
					 */
					
					User u = Main.db.VerifyUserLogin(user_name, user_ps);
					if(u==null)
					{
						sucess = false ;
					}
					else 
					{
						if(u.getName().equals(user_name) && u.getPassword().equals(user_ps))
						{
							sucess = true ;
							user_id = String.valueOf(u.getUserId());
							
						}
					}
					
					/*
					 
					for(User u : MainActivity.user_list.getUser_list())
					{
						if(u.getName().equals(user_name) && u.getPassword().equals(user_ps))
						{
							// lgoin successfull
							sucess = true ;
							user_id = u.getUserId();
							break;
						}
					}
					*/

				}


				String muri_kha = DataFormat.get_cmd_oo1(user_id,(sucess)?"Y":"N" + "\n");
				Log.d("inUserLoginAuth",muri_kha);
				bw.write(muri_kha);
				bw.flush();






				if(!cs.isClosed())cs.close();	

			}
			catch(Exception e)
			{
				Log.d("inUserLoginAuth","Exception!");
				break;
			}
		}
	}
}
