package com.mahfuz.prev.app;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.main.Data;

public class AppToMasterPacketReceiver implements Runnable {

	//private int port  = 2222; 
	private ServerSocket server ;
	public static Boolean isPackHandlerRunning = false ;
	Context context ;
	public String TAG = "AppToMasterPacketHalderThread";
	
	public AppToMasterPacketReceiver(Context con){
		/*************************************************/
			this.context = con ;
			isPackHandlerRunning = true ;
			new Thread(this).start();
		/*************************************************/
	}
	
	@Override
	public void run() {
		
		try{
			server = new ServerSocket(Data.port_AppToMasterPacketReceiver);
			
		}catch(Exception e){}
		while(isPackHandlerRunning){
			try{
			/***********************************************************************/	
				Socket cs = server.accept();
				//ObjectInputStream in 	= new ObjectInputStream(cs.getInputStream());
				
				BufferedReader br = new BufferedReader(new InputStreamReader(cs.getInputStream()));
				//BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(cs.getOutputStream()));
				String pac = br.readLine();
				Log.d(TAG, pac);
				
				Intent intent = new Intent("app");
				intent.putExtra("DATAIN", pac);
				context.sendBroadcast(intent);
								
			
			}catch(Exception e){
				//MainActivity.main.show_tx1("Exception in packet controller thread" + e.toString());
			}
		}
	}
}