package com.mahfuz.prev.app;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import com.example.main.Data;
import com.example.main.Main;

public class MasterToAppCloseThread implements Runnable {
	
	
	final static String TAG = "MasterToAppCloseThread";
	
	public static Boolean isMasterAppCloseThreadRunning = false ;
	private ServerSocket sc = null ;
	private Socket		 cs = null ;
	//private int			 pt = 2020 ;
	
	private BufferedReader br = null ;
	//private BufferedWriter ot = null ;
	
	public MasterToAppCloseThread(){
		isMasterAppCloseThreadRunning = true ;
		new Thread(this).start();
	}
	
	@Override
	public void run() {
		try{
			sc = new ServerSocket(Data.port_MasterToAppCloseThread);
		}catch(Exception e){
			return ;
		}
		
		while(isMasterAppCloseThreadRunning){
			try{
				cs = sc.accept();
				br = new BufferedReader(new InputStreamReader(cs.getInputStream()));
				
				
				String datain = br.readLine();
				
				if(datain.equals("zzz")){
					String ip = cs.getInetAddress().getHostAddress();
				//	MainActivity.app_ip_lis.remove(ip);
				//	MainActivity.main.show_tx1("A app connection is closed");
					
					Main.active_app_list.remove(ip);
					
					
				}	
			}catch(Exception e){
				
			}
			
		}
	}
}
