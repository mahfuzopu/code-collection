package com.mahfuz.threads;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;

import android.util.Log;

import com.example.main.Data;
import com.example.main.Main;
import com.mahfuz.active.ActiveRoom;
import com.mahfuz.connection.DataHandler;
import com.mahfuz.room.Device;
import com.mahfuz.room.Room;

public class RoomToMasterInitThread implements Runnable {

	private static ServerSocket server = null ;
	private BufferedReader in	= null ;
	static public Boolean roomToMasterInitThreadRunning = false ;
	public final String TAG = "RoomToMasterInitThread"; 

	public RoomToMasterInitThread(){

		roomToMasterInitThreadRunning = true ;

		/**************************************/
		new Thread(this).start();

		/**************************************/

	}
	@Override
	public void run() {
		try{
			if(server==null){
				server = new ServerSocket(Data.port_room_master_device);
			}
			else {
				if(!server.isClosed()){
					server.close();
					server = new ServerSocket(Data.port_room_master_device);
				}
			}
		}catch(Exception e){ Log.e(TAG,"Error in creating room to init thread!");}

		while(roomToMasterInitThreadRunning){

			try{
				Log.e("Room to Init Master","waiting for a new room....");


				Socket cs = server.accept();

				in = new BufferedReader(new InputStreamReader(cs.getInputStream()));
				BufferedWriter out = new BufferedWriter(new OutputStreamWriter(cs.getOutputStream()));

				Log.e(TAG,"A New Room");

				String str = in.readLine();

				Log.e(TAG,str);

				Thread.sleep(300);

				out.write("xxx");	//means ack
				out.flush();

				Log.e(TAG,"xxx written!");


				//str = in.readLine();
				
				Thread.sleep(300);
				
				String ip  = cs.getInetAddress().getHostAddress();

				if(!cs.isClosed())cs.close();

				Log.e("Room to Init Master","Socket Closed!");
				
				init_thread(str,ip);

			}catch(Exception e){
				Log.e("Room Master Init Exception","Here Exception!" + e.toString());
			}
		}
	}
	public void init_thread(final String str,final String ip){
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				init_room_device(str,ip);
			}
		}).start();
	}
	public void init_room_device(String str,String ip){




		if(str.startsWith("*HELLO*")){

			String subString = str.substring(7);

			StringTokenizer strtok = new StringTokenizer(subString,":,");


			/*****************************Room Init *****************************/

			String room_id   = strtok.nextToken();
			String room_name = strtok.nextToken();

			ActiveRoom room = new ActiveRoom();
			room.setId(Integer.parseInt(room_id));
			room.setIp(ip);
			room.setName(room_name);

			Main.active_room_list.addRoom(room);

			Log.e(TAG, "A room is adder to active room list");



			//Room r = new Room();


			Room r = new Room();
			r.setIp(ip);
			r.setId(Integer.parseInt(room_id));
			r.setName(room_name);
			r.setActive(1);





			int rrr = Integer.parseInt(room_id);

			if(rrr==0)
			{
				/*
				 * delete the device and room first then insert new room and device
				 */
				//Main.db.DeleteAllDeviceinRoom(rrr);
				//Main.db.DeleteRoom(rrr);
				/*
				 * now insert
				 */
				int r_id = (int)Main.db.createRoom(r);
				r.setId(r_id);
				String r_r_r = "0000" + String.valueOf(r_id);
				int len =r_r_r.length();
				String t_r_r_r = r_r_r.substring(len-4, len);
				DataHandler.sendData("X"+t_r_r_r, ip, 2000);
				String ll = "";

				while(strtok.hasMoreTokens())
				{

					String name = strtok.nextToken();
					String id	= strtok.nextToken();
					String on	= strtok.nextToken();
					String other= strtok.nextToken();
					////////////////////////////////////////////////

					int dev_id = Integer.parseInt(id);
					int onf = Integer.parseInt(on);

					Device d = new Device();
					d.setId(dev_id);  d.setName(name);  d.setType("Light");  d.setOther(other);  d.setOnf(onf); d.setRoomid(r_id);

					if(Main.db.createDevice(d)>0)
					{
						Log.e("Device inserted!","with device id" + d.getId());
					}
					//d.setId(i);

					//Main.db.assigneDevieToRoom(d);

					//		r.add_device(d);

					ll += name + id + on + other + "\n";

					
				}
				Log.e("Device String", ll);
			}
			else
			{
				//active the room and update the device set
				Main.db.setRoomActive(rrr);
				
				String ll = "";

				while(strtok.hasMoreTokens())
				{

					String name = strtok.nextToken();
					String id	= strtok.nextToken();
					String on	= strtok.nextToken();
					String other= strtok.nextToken();
					////////////////////////////////////////////////

					int dev_id = Integer.parseInt(id);
					int onf = Integer.parseInt(on);

					Device d = new Device();
					d.setId(dev_id);  d.setName(name);  d.setType("Light");  d.setOther(other);  d.setOnf(onf); d.setRoomid(rrr);

					Main.db.UpdateDeveiceInfor(d);
					
					//Main.db.createDevice(d);
					//d.setId(i);

					//Main.db.assigneDevieToRoom(d);

					//		r.add_device(d);

					ll += name + id + on + other + "\n";

					Log.e("updated Device String", ll);

				}

				Log.e(TAG, "A room is made active with id = " + room_id );
			}
			
			//update the device proterties and room properties
			Main.uiupdate.update_device_list("", false);
			Main.uiupdate.update_room_list("", false);
			/*
			if(ip.equals("192.168.43.101")){ 
				r.setId("0000");
				r.setName("Living room");
			}
			else if(ip.equals("192.168.43.179")){
				r.setId("0001");
				r.setName("Bed Room");
			}
			 */

			//	MainActivity.main.show_tx1("room id::" + r.getId());


			/******************************* add Device to room **********************************************/
			// here can be duplicate check
			// if any one is disconnected accidently or need to update the device info
			// then this checking might be helpful
			// but in general case it is not needed


			///////////////////////////////////////////////////////////////////////////////////////////
			/*********************************add room to controller **********************************************/

			//	Room t_rm = MainActivity.controller.getRoomBy_ID(room_id);

			//	if(t_rm==null)
			{
				//		MainActivity.controller.add_room(r);
				//		MainActivity.main.show_tx1("a new room added!" + room_id);

				/*****************************************************/
				//Notification.NotifyAll("New Room Found!");

				//		Notification.NotifyAll("C1"+"NR:" + room_id + ":" + room_name+":");
				//		create_a_thread_for_monitor(room_id);
				/*****************************************************/

			}
			//	else 
			{
				//		MainActivity.controller.remove_room(t_rm);
				//		MainActivity.controller.add_room(r);
				//		MainActivity.main.show_tx1("room updated!");
			}

			//MainActivity.main.show_tx3(ll);		
		}
		else 
		{
			//MainActivity.main.show_tx3(str);
		}
	}

	void create_a_thread_for_monitor(String room_id)
	{
		//new RoomToMasterMonitorThread(6666 + Integer.parseInt(room_id));
	}
}
