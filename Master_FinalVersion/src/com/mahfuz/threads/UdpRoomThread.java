package com.mahfuz.threads;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import android.util.Log;

import com.example.main.Data;
import com.example.main.Main;
import com.mahfuz.active.ActiveRoom;
import com.mahfuz.connection.DataHandler;

public class UdpRoomThread implements Runnable{
	
	public final String TAG = "UdpRoomThread";
	public static Boolean isUdpRoomThreadRunning = false ;
	
	public UdpRoomThread()
	{
		isUdpRoomThreadRunning = true ;
		new Thread(this).start();
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		DatagramSocket ds = null; 			
		int server_port = 55555;
		byte[] message = new byte[200];
		DatagramPacket p = new DatagramPacket(message, message.length);
		
		try {
			ds = new DatagramSocket(server_port);
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			isUdpRoomThreadRunning = false ;
		}
		
		while(isUdpRoomThreadRunning)
		{
			try
			{
				ds.setSoTimeout(1000 * 20 * 1);
				ds.receive(p);// set timeOut to 2000ms
				
				final int 			 port 	= p.getPort();
				final String	 	 ip		= p.getAddress().getHostAddress();
				
				if(Main.active_room_list.containsIp(ip))
				{
					//contains 1. send U to room controller
					Log.d(TAG, "sending K");
					String msg = Main.MASTER_IP + ":" + Data.port_room_master_device;
					DataHandler.sendData(msg, ip, port);
					
					int size = Main.active_room_list.getList().size();
					
					for(ActiveRoom ar:Main.active_room_list.getList())
					{
						ar.resetCountdown( size*2);
					}
					
				}
				else
				{
					// does not contain
					Log.d(TAG, "room is not in active list.sending master ip port");
					
					String msg = Main.MASTER_IP + ":" + Data.port_room_master_device;
					DataHandler.sendData(msg, ip, port);
					
				}
				//decrement the counter by one
				
				for(ActiveRoom r:Main.active_room_list.getList())
				{
					int count = r.decrementCountdown();
					
					// check if countdown is zero
					
					if(count==0)
					{
						// room became inactive
						Main.active_room_list.removeRoom(r);
						Log.e(TAG, "A room is removed from active room list");
						//make change in DB
						//set room inactive in DB
						Main.db.setRoomInactive(r.getId());
						Log.d(TAG, "A room is deactivated id= " + r.getId());
					}
					
				}
				
			}
			catch(SocketTimeoutException e)
			{
				// clear all the list and set inactive all the room inactive in database
				Main.active_room_list.removeAllRooms();
				Log.e(TAG, "All removed from active room list");
				/*
				 * DB update
				 */
				Main.db.setAllRoomDeactive();
				
			}
			catch (Exception e) 
			{
				
			}
			
		}
	}
}
