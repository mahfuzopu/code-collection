package com.mahfuz.threads;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import android.content.Intent;
import android.util.Log;

import com.example.main.Data;
import com.example.main.Main;

public class RoomToMasterMonitorThread implements Runnable{

	final String TAG = "RoomToMasterMonitorThread";
	public static Boolean isRoomToMasterMonitorThreadRunning = false ;
	ServerSocket sc = null;
	Socket cs = null ;

	public RoomToMasterMonitorThread()
	{
		isRoomToMasterMonitorThreadRunning = true ; 
		new Thread(this).start();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try{
			if(sc!=null)
			{
				if(!sc.isClosed())sc.close();
			}
			sc = new ServerSocket(Data.port_room_master_monitor);

			while(isRoomToMasterMonitorThreadRunning)
			{
				cs = sc.accept();
				BufferedReader in  = new BufferedReader(new InputStreamReader(cs.getInputStream()));
				//BufferedWriter out = new BufferedWriter(new OutputStreamWriter(cs.getOutputStream()));

				final String datain = in.readLine();
				Log.d(TAG, datain);

				if(!cs.isClosed())cs.close();

				/*
				 * process the incoming request.
				 */

				new Thread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						/*
						 *  broad cast the message and other processing 
						 */
						Intent it = new Intent("room");
						it.putExtra("DATAIN", datain);
						Main.main.sendBroadcast(it);
						Log.e(TAG,datain);

					}
				}).start();

			}

		}catch(Exception e)
		{
		}
	}

}
