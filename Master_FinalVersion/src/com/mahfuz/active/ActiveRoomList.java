package com.mahfuz.active;

import java.util.ArrayList;
import java.util.List;

public class ActiveRoomList {
	
	List<ActiveRoom> lv = new ArrayList<ActiveRoom>();
	
	public ActiveRoomList() {
		// TODO Auto-generated constructor stub
		lv = new ArrayList<ActiveRoom>();
	}
	public List<ActiveRoom> getList()
	{
		return this.lv;
	}
	public void addRoom(ActiveRoom r)
	{
		lv.add(r);
	}
	public void removeRoom(ActiveRoom r)
	{
		for(ActiveRoom rr: lv)
		{
			if(rr.getId() == r.getId())lv.remove(rr);	
		}
	}
	public Boolean containsIp(String ip)
	{
		for(ActiveRoom rr: lv)
		{
			if(rr.getIp().equals(ip))return true;	
		}
		return false ;
	}
	public Boolean containsId(int id)
	{
		for(ActiveRoom rr: lv)
		{
			if(rr.getIp().equals(id))return true;	
		}
		return false ;
	}
	public void removeAllRooms() {
		// TODO Auto-generated method stub
		lv.clear();
	}
}
