package com.mahfuz.active;

public class ActiveRoom {
	int id ;
	String name ;
	String ip ;
	int countdown;
	
	public ActiveRoom() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int decrementCountdown() {
		this.countdown--;
		return this.countdown;
	}

	public void resetCountdown(int countdown) {
		this.countdown = countdown;
	}
}
